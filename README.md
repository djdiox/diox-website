# Diox Website 🚀 🦈
[![pipeline status](https://gitlab.com/djdiox/diox-website/badges/master/pipeline.svg)](https://gitlab.com/djdiox/diox-website/commits/master)
[![Website](https://img.shields.io/website-up-down-green-red/http/shields.io.svg?label=my-website)](https://diox-website.firebaseapp.com/)
[![Version](https://img.shields.io/badge/Version-0.2.20-green.svg)](https://gitlab.com/djdiox/diox-website)



[![Documentation Coverage](https://djdiox.github.io/diox-website/doc/images/coverage-badge.svg)](https://djdiox.github.io/diox-website/docs/)
[![codebeat badge](https://codebeat.co/badges/0a0171e9-0ad4-47f6-b6fc-d82f73a26aaa)](https://codebeat.co/projects/gitlab-com-djdiox-diox-website-master)
[![Documentation Status](https://readthedocs.org/projects/diox-website/badge/?version=latest)](https://diox-website.readthedocs.io/en/latest/?badge=latest)

[![ForTheBadge uses-html](http://ForTheBadge.com/images/badges/uses-html.svg)](http://ForTheBadge.com)
[![ForTheBadge uses-css](http://ForTheBadge.com/images/badges/uses-css.svg)](http://ForTheBadge.com)
[![ForTheBadge uses-js](http://ForTheBadge.com/images/badges/uses-js.svg)](http://ForTheBadge.com)
[![ForTheBadge makes-people-smile](http://ForTheBadge.com/images/badges/makes-people-smile.svg)](http://ForTheBadge.com)
[![ForTheBadge built-by-developers](http://ForTheBadge.com/images/badges/built-by-developers.svg)](https://GitHub.com/Naereen/)

The page ist hosted on [GitLab](https://djdiox.gitlab.io/diox-website/#/home) and built on Angular 6. 

A new version is also available on [Google's Firebase](https://diox-website.firebaseapp.com/#/home)!
It will be soon available on a Web Server with an domain.

You can find out more about this project on the [documentation page](https://djdiox.gitlab.io/diox-website/docs/) and [current unit test coverage](https://djdiox.gitlab.io/diox-website/coverage/).

## Documentation 📝

The files `README.md` and `CHANGELOG.md` 
are the start point for this project and located in the project root directory.

The [Documentation](https://djdiox.gitlab.io/diox-website/docs/) 
is powered by [compodoc](https://github.com/compodoc/compodoc)! 

Just hit `yarn generate:documentation` and 
it will create the folder based on Code, Comments etc.
It will also show the documentation coverage. 

```yarn generate:coverage``` will create a 
material coverage html files in a folder, 
so you can easily access them on a web server.

### Compatibility

This app is best compatible with Google Chrome or any other Chromium based browser.
It does also run on Mozilla Firefox, IE Edge und IE 10

# Development ⚒

This Frontend is currently standalone.
After it has been built it can be served by an HTTP Server e.g. [nginx](https://www.nginx.com/).

> _Tip:_ If you use an Backend like Express.JS you can serve the files with it, instead of using another server.

## Requirements 💊

You will need following tools to develop this project:
- [Node.JS LTS v8.11.1](https://nodejs.org)
- *preferred* installing Node.JS via nvm for more details see [nvm for unix/osx](https://github.com/creationix/nvm) or [nvm for windows](https://github.com/coreybutler/nvm-windows)
- [Yarn v1.9.2](https://yarnpkg.com/lang/en/)
- [Angular CLI v6.1.5](https://github.com/angular/angular-cli)
- [Git (latest)](https://git-scm.com/)
- [firebase tools](https://console.firebase.google.com/u/0/project/diox-website/hosting)
- [Docker (latest)](https://www.docker.com/)

1. Install all dependencies
2. Install dependencies with `yarn`.
3. Copy `src/environments/environment.example.ts` to `src/environments/environment.ts`
4. Fill out needed informations in the environment.ts file.
5. Running `yarn start` wil start up development server on [http://localhost:4200](http://localhost:4200).

> *Tip:* If you want to access the project for instance on a smartphone or tablet in your network 
>> For building `src/environments/environment.prod.ts` is being used

```bash
ng serve --host=0.0.0.0
```

> Usually eth0 is the default IP you need to call like `172.30.30.160:4200`

You can also simply debug prod build using `yarn start:prod`
For more Information about using @angular/ci visit [here](https://github.com/angular/angular-cli/wiki)!

Pushing to any remote runs ``husky`` before that push happens.
This is a called a `git-hook` and can be defined in the package.json
- For more available hooks to define checkout the [Git Page](https://git-scm.com/docs/githooks)
It will run the Tests and linting the TypeScript files.

### Creating a new Patch / Version 💎

Running `yarn version --new-version patch / minor / major` 
will create the semver version.

This cmd will update the version 
located in the `package.json` and it will also generate tag in git. 

For pushing the commit and the tags use `git push --follow-tags`.

> The execution of `git push --tags`
> will just push the tags and not the version in the `package.json`

Execute `yarn build` for a normal build into dist folder.

## Docker 🐳

There is a base docker image available in the [GitLab Container's Registry](https://gitlab.com/djdiox/diox-website/container_registry)
It uses a pre configured karma docker image, which is based on xcvfb and chrome. 
It's modified in order to use yarn as package manager.
If that is done sucessfully, the unit & e2e will run. 

There are few optins in `package.json` for using Docker with this Project.

- `docker:build` for creating base image for the Build
- `docker:test` to run the tests in docker

### Building / Updating the base docker image 🔨🐳

First log in to GitLab’s Container Registry using your GitLab username and password. 
If you have 2FA enabled you need to use a personal access token:

`docker login registry.gitlab.com`

You can also use a deploy token for read-only access to the registry images.

Once you log in, you’re free to create and upload 
a container image using the common build and push commands.
```bash
yarn docker:build
docker push registry.gitlab.com/djdiox/diox-website
```

This will create and override the container used as base for the build.

### Build 🛠️

Currently the automatic build is configured via `gitlab-ci.yml`. 

You can also run the build manually on your system with `yarn build:script`

This will take the base image with the dependency for a complete build:

1. Installing all dependencies with yarn
2. Running Unit tests & generating coverage
3. Run linting tool
3a. _(Coming Soon) Create and run e2e tests_
4. Create new documentation for the code out of the comments
5. Run Node.js script for setting environment variables
  1. Open `src/environments/environment.example.ts`
  2. Scann all values that need to be set in GitLab tags in braces e.g. [IMPORTANT_TOKEN] 
  3. Set Tokens and write File to `src/environments/environment.prod.ts`
6. Generating the Angular production build into GitLab Pages in `public/`
  1. put the documentation in `public/docs/`
  2. put the unit test coverage results in `public/coverage`

Finally it builds all necessary files into `public/` folder and delivers it as artifcats.
GitLab will store the artifacts for 14 days as Zip File in the Build of [GitLab Builds](https://gitlab.com/djdiox/diox-website/pipelines).

## CI / CD 🥇

Continous Integration will be available over 
[GitLab Jobs](https://gitlab.com/djdiox/diox-website/-/jobs). 
And will publish to GitLab Pages.

> All related scripts for the continous integration build can be found
> in the `deployment\` folder


| Folder | Description |
| -------- | -------- |
| base   | Base Dockerimage, that will be used for building.  |
| prod   | Production buildscript and other files   |
| scripts   | Other deployment tools, scripts etc.  |
| staging   | Staging buildscript and other files |


## Firebase 🔥

As database is Firebase from Google used. The credentials for it are stored in the environment.prod.env
More Informations follows soon, the code will be updated, in order to make the blog work.

In order to deploy to firebase you'll need access to diox-website's google cloud and a google account.
In this [Tutorial](https://codelabs.developers.google.com/codelabs/firebase-web/?authuser=0#0),
all dependencies and commands are listed.

For this app it is enough to install firebase-tools with `yarn add global firebase-tools`
Afterwards you can login with `firebase login`. 
If you run then `yarn firebase:deploy` 
it will deploy the current generated build in the local `public\` folder. 

## Authentication 🔐

Will be available through firebase.
Login Function currently works if you press anywhere on the webpage `L`.

For an example experience you can login with email `test@example.com` and password `test`

# Feature Overview 💻

Since the base version (not mobile yet) is working fine: 

See `TODO.md` For more Information available here [GitLab](https://gitlab.com/djdiox/diox-website/blob/master/TODO.md)

# About Angular 💥

Thd scaffold was generated with [Angular CLI](https://github.com/angular/angular-cli) (>v6.0.8).

[Angular-CLI GitHub CvPage](https://github.com/angular/angular-cli)


## Updating Angular / CLI etc. 🔧

*ATTENTION:** This might break the system, since the versioning of npm / ng is a big mess. 
Sometimes the dependencies will not match each other and then you need to fix it manually


1. Running `ng -v` in the Project root after having installed Angular CLI will print out both the installed global and local package version.
2. If you decide then to update angular or angular/cli run `ng update` in the Project root will show any available Updates for the current version.
3. Now run `ng update --all` and angular-cli will try to fix all the open issues automaticly. 

> _Tip:_ For big version updates, it will happen that there are breaking changes in the Patches.

But for that Problem Google made 
[Angular Update Guide](https://update.angular.io/) to see what you need to change for the update.

---

To get more help on the Angular CLI use `ng help` 
or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


# Useful links 🌐

- Angular
  - [Learn RxJS](https://www.learnrxjs.io/)
  - [Angular Blog](https://blog.angular.io/)
  - [Angular Resources](https://angular.io/resources)
    - [Angular Material](https://material.angular.io/)
    - [Material Icons (Font Awesome)](https://material.io/tools/icons)
  - [Tons of resources on GitHub](https://github.com/gdi2290/awesome-angular)
  - [Jasmine Cheat Sheet](https://devhints.io/jasmine)
- GitLab / Documentation
  - Test
  - [GitLab markdown documentation](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/#warnings)
  - [md synthax highlight](https://support.codebasehq.com/articles/tips-tricks/syntax-highlighting-in-markdown)
  - [Emojicopy](https://www.emojicopy.com/)
- Docker
  - [Angular CLI Image](https://github.com/trion-development/docker-ng-cli-karma)
  - [Docker Cheat Sheet](https://github.com/wsargent/docker-cheat-sheet)
- Dev Tools / Knowledge
  - [Webstorm](wwww.jetbrains.com)
  - [dev.to](www.dev.to)
  - [StackBlitz](https://stackblitz.com)
  - [Cmder](http://cmder.net/)
  
# About the developer 🤵

Stuttgart-Based developer running a dj/producer project. Love music, coding & peace

Connect on [Facebook](https://www.facebook.com/djdi0x/) or [Twitter](https://twitter.com/markuswagner93)

(c) 2018 Markus Wagner
