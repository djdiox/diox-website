## TODO List

Here you can find a list of features that are planned, or just started to be implemented:

<span style="color:blue">IN PLANNING / KICK OFF</span>

- [ ] Keep Sharky 🦈 Story!
- [ ] Enable Themes
    - [ ] Add low bandwidth theme
    - [ ] Add Techno Theme
- [ ] Blog - 10%
- [ ] End to End Tests - 0%
- [ ] Node.JS Backend instead of firebase
- [ ] Live Updates from Twitter, Facebook & Instagram
- [ ] Comment Section
- [ ] Rework CV
- [ ] Keep Angular up2date (current v6.1.5)
- [ ] Add Read the Docs Documentation


<span style="color:green">IN PROCESS</span>

Features that are nearly done, mostly of it is already in available In the App:

- [x] Advanced Chat Bot interaction / help 80%
- [x] Complete Documentation - 60%
- [x] Unit Tests - 70%
- [x] Mobile working - 80%
- [x] Chatbot - 40%
- [x] Login - 90%
- [x] Customizable Settings for Logged In User - 50%
- [x] Expose secret variables e.g. token to environment.ts before Build - 100 %
