const fs = require('fs-extra');
const cp = require('child_process');


const configPath = 'src/environments/environment.firebase.ts';
const packageJson = fs.readJsonSync('./package.json');

(async () => {
  let nextVersion = '';
  try {
    const prodConfig = await fs.readFile(configPath, 'utf-8');
    const versionIndex = prodConfig.indexOf('version: \'') + 10;
    const versionEndIndex = prodConfig.indexOf('\'', versionIndex);
    const latestVersion = prodConfig.substring(versionIndex, versionEndIndex);
    nextVersion = packageJson.version;
    console.log('Latest version before this update was:', latestVersion);
    console.log('After the update the version will be', nextVersion);
    let currentProdConfig = await fs.readFile(configPath, 'utf-8');
    const nextConfig = currentProdConfig.replace(latestVersion, nextVersion);
    await fs.writeFile(configPath, nextConfig, 'utf-8');
  } catch (e) {
    console.error(e);
  } finally {
    cp.execSync('CI_PROJECT_NAME=diox-website CI_COMMIT_TAG=' + nextVersion + ' ')
    process.exit(0);
  }
})();
