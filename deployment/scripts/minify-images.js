const compress_images = require('compress-images');
const fs = require('fs-extra');
const compressImages = async (input) => {

  const files = await fs.readDir()
  console.log('Converting Files:', files);
  return new Promise((resolve, reject) => {
    compress_images(input, input, {
        compress_force: false,
        statistic: true,
        autoupdate: true
      }, false,
      {
        jpg: {
          engine: 'mozjpeg', command: ['-quality', '60']
        }
      },
      {
        png: {
          engine: 'pngquant', command: ['--quality=70-80']
        }
      },
      {
        svg: {
          engine: 'svgo', command: '--multipass'
        }
      },
      {
        gif: {
          engine: 'gifsicle', command: ['--colors', '64', '--use-col=web']
        }
      }, function (err, res) {
        return err ? reject(err) : resolve(res);
      }
    );
  });
};
(async () => {
  await Promise.all(
    process.argv[2].split(',')
      .map(folder => compressImages(folder))
  );
  console.log(done);
})();
