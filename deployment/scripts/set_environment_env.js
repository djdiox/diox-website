const fs = require('fs-extra');
const examplePath = 'src/environments/environment.example.ts';
const configPath = 'src/environments/environment.prod.ts';
const environmentVariables = [
  'DIALOGFLOW_TOKEN',
  'CI_COMMIT_TAG',
  'FIREBASE_TOKEN'
];

console.log('Setting GitLab Private variables to Angular via "' + configPath + '" for production build.');

fs.readFile(examplePath, 'utf8', (err, exampleConfig) => {
  if (err) {
    return console.error(err);
  }
  const config = environmentVariables.reduce((config, env) => {
    console.log(`Setting "${env}" value from GitLab to Angular`);
    const value = process.env.hasOwnProperty(env) ? process.env[env] : 'NOT_FOUND';
    if(value === 'NOT_FOUND') {
      console.error('Could not set environment variables for key "' + env + '"');
    }
    const current = config.replace(`[${env}]`, value);
    return current;
  }, exampleConfig);
  fs.writeFile(configPath, config, 'utf8', err => {
    if (err) {
      console.error(err);
    }
    process.exit(0);
  })
});
