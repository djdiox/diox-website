#!/usr/bin/env bash

# Variables for this script
BUILD_DIR="./dist/"
FINAL_DIR="./public/"

echo "cleaning target dir ${FINAL_DIR}, and ./coverage/ before generating build again..."
rm -rf "${FINAL_DIR}**/**"
rm -rf "./coverage/";
echo "done."

echo "installing dependencies with yarn..."
yarn
if [ -e ./yarn-error.log ]; then
    echo "File not found!"
    cat yarn-error.log
    exit
fi

ng -v
echo "Running tests for coverage: ${CI_PROJECT_NAME} on ${CI_COMMIT_TAG} is starting up (By: ${GITLAB_USER_NAME}) "
yarn generate:coverage
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

yarn lint

rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

echo "Running script for setting private variables..."
yarn config:set

echo "Everything looks fine... Continuing with generating GitLab Build: "
yarn build
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi
echo "Build for version: ${CI_COMMIT_TAG} into ${BUILD_DIR} was successful!!"

echo "Creating documentation for this folder in ${FINAL_DIR}docs/"
yarn generate:documentation

echo "Copying coverage files into public folder..."
mkdir -p "${FINAL_DIR}coverage"
cp -R ./coverage/* "${FINAL_DIR}coverage"

echo "*Workaround for sometimes not correct working GitLab Pages deploy:"
echo "Copying files build from ${BUILD_DIR}. to  into public (${FINAL_DIR}) folder..."
cp -a "${BUILD_DIR}." "${FINAL_DIR}"

echo "All Steps has been finished, ${FINAL_DIR} directory looks like:"
ls -alhtr "${FINAL_DIR}"

echo "Build is now ready to serve on https://djdiox.gitlab.io/diox-website/"

rm -f ./yarn-error.log

