import {UserMock} from './mock-angular-fire';

/**
 * A Mocked Instance of the Service
 */
export class MockAuthService {
  /**
   * Creates the instance of the mock
   */
  constructor() {}
  /**
   * Gets the latest auth state as an Object
   * @constructor  {AuthChanged} Current Auth Changes that are available project wirde
   */
  public LastAuthChanges = {
    loggedIn: false,
    currentUser: UserMock
  };
  /**
   * Gets the Observable for showing the loginModal
   * @constructor {Observable<boolean>} The observable of the Show Login Modal
   */
  public LoginModalToggled = {
    subscribe: jasmine.createSpy('subscribe', function (cb)  {
      cb(this._authChanged);
    }).and.returnValue({
      unsubscribe: jasmine.createSpy(),
      subscribe: jasmine.createSpy()
    })
  };
  /**
   * gets the Observable of the current Changes that are client wide in the App
   * e.g. login, setting changed etc.
   * @constructor {Observable<AuthChanged>} AuthChanged Observable
   */
  public AuthChanged = {
    subscribe: jasmine.createSpy('subscribe', function (cb)  {
      cb(this._authChanged);
    }).and.returnValue({
      unsubscribe: jasmine.createSpy(),
      subscribe: jasmine.createSpy()
    })
  };
  /**
   * Gets wether the user is logged in or not
   */
  public isLoggedIn = jasmine.createSpy('loggedIn').and.callFake(function () {
    return this.LastAuthChanges.loggedIn;
  });
  /**
   * Saves a setting into the User
   * @param newSettingValues {any} some settings to save
   */
  public assignSettings = jasmine.createSpy('assignSettings').and.returnValue({
    subscribe: jasmine.createSpy(),
    unsubscribe: jasmine.createSpy(),
    toPromise: jasmine.createSpy().and.returnValue(Promise.resolve())
  });
  public restoreSession =  jasmine.createSpy('restoreSession', () => false);
  /**
   * Login Callback
   * @returns {boolean} has the action been executed?
   */
  public showLogin = jasmine.createSpy('showLogin');
  /**
   * Mock Google Login
   */
  public loginFromGoogle = jasmine.createSpy('loginFromGoogle').and.returnValue(Promise.resolve());
  /**
   * Restes LastAuthChanges
   */
  public reset() {
    this.LastAuthChanges = {
      loggedIn: false,
      currentUser: UserMock
    };
  }
}
