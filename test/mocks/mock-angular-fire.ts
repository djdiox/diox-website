import {Settings} from '../../src/app/classes/models/settings';
/**
 * A mocked User
 */

export const UserMock = {
  email: 'user@example.org',
  uid: '12345',
  displayName: 'Max Mustermann',
  settings: new Settings({user_id: this.uid})
};

/**
 * The mock of angular fire Auth
 */
export const MockAngularFireAuth = {
  auth: jasmine.createSpyObj('auth',
    ['signInAndRetrieveDataWithEmailAndPassword'])
};
/**
 * Since AngularFire builds with functions (old js classes),
 * we mock the function on the object directly
 * rejects if the mail is not the same as the user Mock
 */
MockAngularFireAuth.auth
  .signInAndRetrieveDataWithEmailAndPassword = async (mail) => {
  return new Promise((resolve, reject) => {
    if (mail !== UserMock.email) {
      return reject('wrong mail');
    }
    resolve({user: UserMock});
  });
};
