/*
 * Gets a Helper that reduces boilerplate code on for the unit tests
 */
export const TestHelper = {

  /**
   * Creates a app for the unit tests
   * @param TestBed {object} The TestBed with Mocks from Angular
   * @param component {object} the Component that is being tested
   * @returns {object} The component Instance of the new App
   */
  createApp: (TestBed: any, component: any) => {
    const fixture = TestBed.createComponent(component);
    return fixture.debugElement.componentInstance;
  }
};

