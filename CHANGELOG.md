# Changelog 📜

All changes saved with version and changes in the past are documented here:

## Vesion 0.2.26

- Added Scroll Navigation 

## Version 0.2.25

- Fix Sharky on build
- fixed mobile view
- adjusted firebase build script

## Version 0.2.22 - 0.2.24

- Build workaround for sharky
- integrated firebase to build
- Fixed Animations
- Fixed missing SVG's after build

## Version 0.2.21

- Try to repair sharky, after prod build animation does not work anymore

## Version 0.2.20

- Updated Badges on Readme

## Version 0.2.19

- Moved App to firebase hosting
- changed auth domain back to diox-website.firebaseapp.com
- Initialized firebase in project.
- Added Tutorial for deploying to firebase
- added set of version from Tag to package.json for firebase

## Version 0.2.18

- Added Domain djdiox.gitlab.io for oAuth (Google) to environment files

## Version 0.2.16 && 0.2.17

- Fixed Config script again
- Hard traceable error on setting config fixed

## Version 0.2.15

- Fixed Compodoc again
- Added ChatHelp Page
- Show Chat Help Modal on Button Click
- Try out different Chat functions
- Created unit tests for it.
- improved config script

## Version 0.2.14

- Added Unit Tests for Social Icons
- Fixed Icon Behaviour
- Moved social icons transition to keyframe animation

## Version 0.2.13

- Added Login From Google Feature
- Refactored Icons Handler to Icons Service
- Linted Files

## Version 0.2.12

- Improved Login for application wide use
- Removed AngularFireAuth from 'L' settings.
- Added oAuth Login to Login Modal

## Version 0.2.11

- fixed missing dependencies in yarn.lock for karma-jasmine
- Adjusted Documentation

## Version 0.2.10

- Fixed Problems with Karma and new Angular-CLI & Angular Version
- Adjusted Shark Action

## Version 0.2.9

- Fixed Missing icon in Build

## Version 0.2.8

- Advanced Bot responses
- Sharky Action!!

## Version 0.2.7

- Removed Background
- Changed Layout
- Updated Angular
  - core 6.1.1 -> 6.1.4
  - material 6.4.2 -> 6.4.6

## Version 0.2.6

- Fixed Player

## Version 0.2.5

- Bugfix of loading screen bg
- added fallback bg color to loading screen

## Version 0.2.4

- Fixed Bug in Chat
- rework of gallery
- added gallery pictures
- improved loading screen

## Version 0.2.3

- Fixed Chat Bug with scrolling
- improved chat design
- added multiple responses for not found answers

>>>>>>> master

## Version 0.2.2

- Fixed error in compodoc, and now changed config for it.

## Version 0.2.1

- Changed Cloud Background
- SEO Optimization
- optimized layout
- fixed documentation (wrong config)

## Version 0.2.0

- Complete design rework
- added cloudy background
- changed mobile view

## Version 0.1.20 && Version 0.1.21
- css fix of chat

## Version 0.1.19
- refactorement & linting error

## Version 0.1.18
- fixed missing typing

## Version 0.1.17
- Added more Commands for Chat

## Version 0.1.16

- Repaired unit tests

## Version 0.1.15

- Created social icons component
- added top mode for social icons
- adjusted design on mobile
- Enabled Dialogflow Bot
  - with ability to change settings

## Version 0.1.14

- Adjusted angular.json for correct coverage
- added unit tests, now more than 80% Statements!

## Version 0.1.12 & 0.1.13

- added unit tests

## Version 0.1.11

- adjusted login

## Version 0.1.10

- adjusted readme and changelog
- fixed task list in readme

## Version 0.1.9

- added logout button

## Version 0.1.8

- adjusted settings with new buttons

## Version 0.1.7

- fixed build script

## Version 0.1.2

- adjusted settings button
- adjusted settings design
- Fixed Bug in Side Nav from Material
- Added Follow Button to Home

## Version 0.1.1
- Refactored Auth Service completely
- Added more unit tests!
- Added Player to the bottom of the Page
- added lint & test at prepush with husky

## Version 0.1.0

- Chat is now working better, but still deactivated
- Build in Settings
- Added favicon and other web specific assets

## Version 0.0.10
- Adjusted README
- More design specific details

## Version 0.0.9

- Added Transition
- Cleaned up project
- Added more unit tests
- integrated toaster
- In Mobile now gets fullscreen navigation
- Added loading spinner
- Changed Gallery

## Version 0.0.8
- App Redesign
- CV Tweaks
- Unit Tests
- Adjusted Build
- Added Complete Build Shell Script
- GitLab is now main hoster
- GitHub is now synced on GitLab
- Unit Tests
- Created Dockerfile for stable building
- Updated Angular von 6.0.8 to 6.1.1
- Included Coverage in Web CvPage
- Added Documentation for the process

## Version 0.0.7
- Added Material Design
- Added Compodoc

## Version 0.0.6
- Added Interactive CV (first version)
- Tweak SCSS

## Version 0.0.5
- Added Possibility to change view for mobile, on hide nav

## Version 0.0.4
- More Bugfixes
- Started Modal Component
- Added GitLab CI/CD Integration for the Project

## Version 0.0.3
- Bugfixes
- Added Changelog
- Edited Readme.md
- Fixed Build
- Updated to Angular-CLI 6.1.0

## Version 0.0.2
- Added Routes
- Tweak nav-bar
- Added content to components
- Begin SASS Structure
- Added GitHub Pages Build


## Version 0.0.1
- Created scafolded version of the app and pages
- Added pages and scrolling service
- Imported Pages in the App and made them available
- Enabled SASS
- Added navbar
