/**
 * The current environment variables, that will change from app to app
 * (deployment to deployment)
 * @type Object
 */
export const environment = {
  production: true,
  dialogFlowToken: '',
  version: '0.2.18',
  firebase: {
    apiKey: '',
    authDomain: 'djdiox.gitlab.io',
    databaseURL: 'https://diox-website.firebaseio.com',
    projectId: 'diox-website',
    storageBucket: 'diox-website.appspot.com',
    messagingSenderId: '143634320556'
  }
};
