/**
 * The current environment variables, that will change from app to app
 * (deployment to deployment)
 * @type Object
 */
export const environment = {
  production: true,
  dialogFlowToken: '[DIALOGFLOW_TOKEN]',
  version: '[CI_COMMIT_TAG]',
  firebase: {
    apiKey: '[FIREBASE_TOKEN]',
    authDomain: 'djdiox.gitlab.io',
    databaseURL: 'https://diox-website.firebaseio.com',
    projectId: 'diox-website',
    storageBucket: 'diox-website.appspot.com',
    messagingSenderId: '143634320556'
  }
};
