/**
 * The current environment variables, that will change from app to app
 * (deployment to deployment)
 * @type Object
 */
export const environment = {
  production: false,
  dialogFlowToken: '2148822a4c1c454495864ecead74be1a',
  version: '0.2.18',
  firebase: {
    apiKey: 'AIzaSyBcYD_2BYYlwmlMZ295MWcqtDEqqhACsdc',
    authDomain: 'djdiox.gitlab.io',
    databaseURL: 'https://diox-website.firebaseio.com',
    projectId: 'diox-website',
    storageBucket: 'diox-website.appspot.com',
    messagingSenderId: '143634320556'
  }
};
