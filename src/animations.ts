import {animate, group, query, style, transition, trigger} from '@angular/animations';

/**
 * The angular animation for the router transitions in this app.
 */
export const routerTransition = trigger('routerTransition', [
  transition('* <=> *', [
    /* order */
    /* 1 */ query(':enter, :leave', style({position: 'fixed'})
      , {optional: true}),
    /* 2 */ group([  // block executes in parallel
      query(':enter', [
        style({transform: 'translateY(100%)'}),
        animate('0.5s ease-in-out', style({transform: 'translateY(0%)', opacity: 0.5}))
      ], {optional: true}),
      query(':leave', [
        style({transform: 'translateY(0%)'}),
        animate('0.5s ease-in-out', style({transform: 'translateY(-100%)', opacity: 1}))
      ], {optional: true}),
    ])
  ])
]);

export const reverseRouterTransition = trigger('reverseRouterTransition', [
  transition('* <=> *', [
    /* order */
    /* 1 */ query(':enter, :leave', style({position: 'fixed', width: '100%'})
      , {optional: true}),
    /* 2 */ group([  // block executes in parallel
      query(':enter', [
        style({transform: 'translateY(100%)'}),
        animate('0.5s ease-in-out', style({transform: 'translateX(0%)'}))
      ], {optional: true}),
      query(':leave', [
        style({transform: 'translateY(0%)'}),
        animate('0.5s ease-in-out', style({transform: 'translateY(-100%)'}))
      ], {optional: true}),
    ])
  ])
]);
