import {inject, TestBed} from '@angular/core/testing';
import {IconsService} from './icons.service';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

const domSanitizerMock = {
  bypassSecurityTrustResourceUrl: jasmine.createSpy('bypass').and.callFake(val => val)
};
const matIconRegistryMock = {
  addSvgIcon: jasmine.createSpy('addSvgIcon')
};
describe('IconsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        IconsService,
        {
          provide: DomSanitizer,
          useValue: domSanitizerMock
        },
        {
          provide: MatIconRegistry,
          useValue: matIconRegistryMock
        }
      ]
    });
  });
  let iconsService;
  beforeEach(inject([IconsService], (service: IconsService) => {
    iconsService = service;
  }));

  it('should be created', async() => {
    await expect(iconsService).toBeTruthy();
    expect(Array.isArray(iconsService.iconNames)).toBeTruthy();
  });
  it('should get a icon', async() => {
    const icon = iconsService.getIconPath('angular');
    await expect(icon).toEqual('assets/images/icons/angular.svg');
  });

  it('should register all the icons', async () => {
    iconsService.registerIcons();
    await expect(matIconRegistryMock.addSvgIcon)
    .toHaveBeenCalledTimes(iconsService.iconNames.length - 1);
  });

});
