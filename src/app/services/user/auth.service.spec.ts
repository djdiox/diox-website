import {inject, TestBed} from '@angular/core/testing';

import {AuthChanged, AuthService} from './auth.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {ToasterService} from 'angular2-toaster';
import {PageService} from '../routing/page.service';
import {MockAngularFireAuth, UserMock} from 'test/mocks/mock-angular-fire';
import {User} from '../../classes/models/user';
import {RouterTestingModule} from '@angular/router/testing';

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        {
          provide: AngularFireAuth,
          useValue: MockAngularFireAuth
        },
        AuthService,
        PageService,
        ToasterService,
      ]
    });
  });

  let authService;
  beforeEach(inject([AuthService], (service: AuthService) => {
    window.localStorage.getItem = () => null;
    authService = service;
    authService.logout();
  }));

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  it('should fail on wrong credentials', async () => {
    let error;
    try {
      await authService.login('tester@test.de', 'pw');
    } catch (e) {
      error = e;
    }
    expect(error).toEqual('wrong mail');
  });

  it('should login with angularFire', async (done) => {
    const fakeLogin = {
      email: UserMock.email,
      password: '123456'
    };
    authService.AuthChanged.subscribe((changes: AuthChanged) => {
      expect(changes.currentUser.email).toEqual(fakeLogin.email);
      expect(changes.currentUser.id).toEqual(UserMock.uid);
      expect(changes.currentUser.name).toEqual(UserMock.displayName);
      done();
    });
    const result = await authService.login(fakeLogin.email, fakeLogin.password);
    expect(result).toEqual(true);
    const exampleResult = await authService.login('test@example.com', 'test');
    expect(exampleResult).toBeTruthy();
  });

  it('should logout', (done) => {
    authService.AuthChanged.subscribe((changes) => {
      expect(changes.loggedIn).toEqual(false);
      done();
    });
    authService.logout();
  });

  it('should assign the settings to the current state', (done) => {
    const newSettings = {
      isChatVisible: true
    };
    authService.LastAuthChanges.currentUser = new User('test');
    authService.LastAuthChanges.currentUser.settings.isChatVisible = false;
    authService.AuthChanged.subscribe(authChanged => {
      expect(authChanged.currentUser.settings.isChatVisible).toBeTruthy();
      done();
    });
    authService.assignSettings(newSettings);
  });

  it('should look for an saved session and return false on not found', () => {
    const isRestoredSession = authService.restoreSession();
    expect(isRestoredSession).toBeFalsy();
  });

  it('should restore the session', () => {
    spyOn(window.localStorage, 'getItem')
      .and.returnValue(JSON.stringify({loggedIn: true, currentUser: UserMock}));
    const isRestoredSession = authService.restoreSession();
    expect(isRestoredSession).toBeTruthy();
  });
});
