import {Injectable} from '@angular/core';
import {User} from '../../classes/models/user';
import {AngularFireAuth} from 'angularfire2/auth';
import {Tools} from '../../classes/tools/tools';
import {Settings} from '../../classes/models/settings';
import {PageService} from '../routing/page.service';
import {Observable, Subject} from 'rxjs';
import * as _ from 'lodash';
import {ToasterService} from 'angular2-toaster/src/toaster.service';

@Injectable({
  providedIn: 'root'
})
/**
 * Handles Authentication Management
 */
export class AuthService {

  /**
   * The key of the local storage item of the logged in User
   */
  private readonly LOCAL_STORAGE_KEY = 'loggedIn_User';

  /**
   * The state, when the auth changes
   */
  private _authState: AuthChanged = {
    loggedIn: false
  };

  /**
   * The instance of the subject for the service
   */
  private _authChanged = new Subject<AuthChanged>();

  /**
   * The Subject for the action if somewhere the user has toggled the login modal
   */
  private _loginModalToggled = new Subject<boolean>();

  /**
   * Saves a setting into the User
   * @param newSettingValues {object} some settings to save
   */
  public assignSettings(newSettingValues: any): Observable<AuthChanged> {
    if (!newSettingValues) {
      return;
    }
    this.changeAuthState({
      currentUser: {
        settings: newSettingValues
      }
    });
    return this._authChanged.asObservable();
  }

  /**
   * Creates a user object by the response from Firebase
   * @param response {Object} Firebase Response
   * @returns {User} a new User
   */
  private createUserObject(response: any): User {
    return {
      id: response.user.uid,
      email: response.user.email,
      name: response.user.displayName,
      lastLogin: new Date(),
      lastClient: Tools.getClient(navigator.userAgent),
      settings: new Settings({
        user_id: response.user.uid,
        currentPage: this.pageService.CurrentPage.view,
        chatVisible: true,
        chatOnline: false
      })
    };
  }

  /**
   * Creates an instance of the AuthService
   */
  constructor(
    private afAuth: AngularFireAuth,
    private toasterService: ToasterService,
    private pageService: PageService
  ) {
    this._authChanged = new Subject<AuthChanged>();
    this._authChanged.subscribe(changes => {
      if (!changes.loggedIn) {
        return;
      }
      window.localStorage.setItem(`${this.LOCAL_STORAGE_KEY}`,
        JSON.stringify(changes));
    });
  }

  /**
   * Changes current Auth State with an desired object
   * Toggles the Observable with the edited State
   * @param newState {object} some parameters that will be changed
   */
  private changeAuthState(newState: any) {
    const nextState = _.merge(this._authState, newState);
    this._authState = nextState;
    this._authChanged.next(nextState);
    return nextState;
  }

  /**
   * Restores a user from the local storage of the browser
   * @returns {boolean} can the User be restored?
   */
  public restoreSession(): boolean {
    if (this.isLoggedIn()) {
      return true;
    }
    const authChangesJSON = window.localStorage.getItem(this.LOCAL_STORAGE_KEY);
    if (authChangesJSON) {
      const nextAuthChanges = JSON.parse(authChangesJSON);
      const newState = this.changeAuthState(nextAuthChanges);
      return newState.loggedIn;
    }
    return !!authChangesJSON;
  }

  /**
   * Login Callback
   * @returns {boolean} has the action been executed?
   */
  public showLogin() {
    if (this.isLoggedIn()) {
      this.toasterService.pop('success', 'Already logged in!');
      return Observable.create(null);
    }
    this._loginModalToggled.next(true);
    return this._loginModalToggled.asObservable();
  }

  /**
   * Logs the user out of the App
   */
  public logout() {
    window.localStorage.removeItem(this.LOCAL_STORAGE_KEY);
    return this.changeAuthState({loggedIn: false, current: null});
  }

  /**
   * Transforms a Google Profile and logs in the user
   * @param response {Response} The Response from Google
   * @returns {Promise<AuthChanged>} a Promise when the action is fulfilled
   */
  public loginFromGoogle(response: any): Promise<AuthChanged> {
    const user = this.createUserObject(response);
    this.changeAuthState({loggedIn: true, currentUser: user});
    return this.AuthChanged.toPromise();
  }
  /**
   * Logs the user into the system
   * @param email {string} a google email address
   * @param password {string} the password for this access
   */
  public async login(email: string, password: string) {
    if (this.isLoggedIn()) {
      return this.isLoggedIn();
    }

    if (email === 'test@example.com' && password === 'test') {
      const user = this.createUserObject({
        user: {
          uid: 'test',
          email: email,
          displayName: 'Test User'
        }
      });
      this.changeAuthState({loggedIn: true, currentUser: user});
    } else {
      const response = await this.afAuth.auth
        .signInAndRetrieveDataWithEmailAndPassword(email, password);
      if (!response) {
        return false;
      }
      await this.loginFromGoogle(response);
    }
    return this.isLoggedIn();
  }

  /**
   * Gets wether the user is logged in or not
   */
  public isLoggedIn(): boolean {
    return this._authState.loggedIn;
  }

  /**
   * gets the Observable of the current Changes that are client wide in the App
   * e.g. login, setting changed etc.
   * @constructor {Observable<AuthChanged>} AuthChanged Observable
   */
  public get AuthChanged(): Observable<AuthChanged> {
    return this._authChanged.asObservable();
  }

  /**
   * Gets the latest auth state as an Object
   * @constructor  {AuthChanged} Current Auth Changes that are available project wirde
   */
  public get LastAuthChanges(): AuthChanged {
    return this._authState;
  }

  /**
   * Gets the Observable for showing the loginModal
   * @constructor {Observable<boolean>} The observable of the Show Login Modal
   */
  public get LoginModalToggled(): Observable<boolean> {
    return this._loginModalToggled.asObservable();
  }
}

/**
 * An class to gather all the auth data in one change event
 */
export interface AuthChanged {
  loggedIn: boolean;
  currentUser?: User;
}
