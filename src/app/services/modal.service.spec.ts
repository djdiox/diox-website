import {inject, TestBed} from '@angular/core/testing';

import {ModalService} from './modal.service';
import {ModalOptions} from '../classes/models/modal-options';

describe('ModalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModalService]
    });
  });

  const mockModal = new ModalOptions('test1', 'test2', 'test3');

  let service;

  beforeEach(inject([ModalService], (_service: ModalService) => {
    service = _service;
  }));

  it('should be created', async () => {
    await expect(service).toBeDefined();
  });

  it('should show the array', (done) => {
    service.CurrentModalOptions
      .subscribe(async (current) => {
        await expect(current.name).toEqual(mockModal.name);
        await expect(current.title).toEqual(mockModal.title);
        await expect(current.content).toEqual(mockModal.content);
        await expect(current.visible).toEqual(true);
        done();
      });
    service.showDefaultModal(mockModal.name, mockModal.title, mockModal.content);
  });
});
