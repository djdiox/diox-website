import {inject, TestBed} from '@angular/core/testing';

import {InteractiveCvService} from './interactive-cv.service';

describe('InteractiveCvService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InteractiveCvService]
    });
  });

  let service;

  beforeEach(inject([InteractiveCvService], (_service: InteractiveCvService) => {
    service = _service;
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set the state', (done) => {
    const newState = {
      test: 'test',
      old: 'test'
    };
    service.CurrentState
      .subscribe((currentState) => {
        expect(currentState.test).toEqual(newState.test);
        expect(currentState.old).toEqual(newState.old);
        done();
      });
    service.setState(newState);
  });

  it('should get current bradcrumbs', () => {
    const breadcrumbs = [
      'test1',
      'test2',
      'test3'
    ];
    const result = service.getBreadcrumbs('test1', breadcrumbs);
    expect(result).toEqual(breadcrumbs);
    const newBreadcrumbs = service.getBreadcrumbs('test4', breadcrumbs);
    expect(newBreadcrumbs).toContain('test4');
  });
});
