import {inject, TestBed} from '@angular/core/testing';

import {DialogFlowService} from './dialog-flow.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import {ApiAiClient} from 'api-ai-javascript';

describe('DialogFlowService', () => {
  let dialogFlowService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DialogFlowService, HttpClientTestingModule],
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ]
    });
  });

  beforeEach(inject([DialogFlowService], (service: DialogFlowService) => {
    dialogFlowService = service;
  }));

  afterEach(inject([HttpTestingController], (backend: HttpTestingController) => {
    backend.verify();
  }));

  it('should be created', inject([DialogFlowService], (service: DialogFlowService) => {
    expect(service).toBeTruthy();
  }));

  it('should get a response from the API AiClient', async () => {
    const spy = spyOn(ApiAiClient.prototype, 'textRequest').and.callFake(() => {
    }).and.returnValue(Promise.resolve());
    await dialogFlowService.getResponse('test');
    expect(spy).toHaveBeenCalledWith('test');
  });
});
