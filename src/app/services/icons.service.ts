import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IconsService {
  /**
   * Base Folder of the icons in the app
   * @type {string}
   */
  private readonly iconBaseFolder: string = 'assets/images/icons/';

  /**
   * The map of icons in the app
   */
  private icons = new Map<string, string>();

  /**
   * All names of the icons
   */
  public iconNames = [
    'angular',
    'docker',
    'earth',
    'facebook',
    'firebase',
    'gears',
    'github',
    'hamburger',
    'instagram',
    'mixcloud',
    'octocat',
    'photoshop',
    'settings',
    'soundcloud',
    'spinner',
    'twitter',
    'ubuntu',
    'school',
    'photoshop',
    'gitlab'
  ];
  /**
   * Registers all Icons in Angular Material
   * These Icons are defined in the array in icons.ts
   * @param iconRegistry {MatIconRegistry} Material Icon Registry
   * @param sanitizer {DomSanitizier} Angular Dom Sanitizer to trust SVG
   */
  public registerIcons = () => {
    this.icons.forEach((iconName, iconPath) =>
      this.iconRegistry.addSvgIcon(
        iconName,
        this.sanitizer
          .bypassSecurityTrustResourceUrl(iconPath)));
  }

  /**
   * Current Icons of the App
   * @type {Map<string, string>} The complete icon collection
   */
  public get Icons(): Map<string, string> {
    return this.icons;
  }

  /**
   * Creates a new Instance of the
   */
  constructor(
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
  ) {
    this.iconNames.forEach(name =>
      this.icons.set(name, `${this.iconBaseFolder}${name}.svg`));
  }

  /**
   * Gets a Icon by the name from the Map
   * @param name {string} the search parameters for all icons on the App
   * @returns {string} path of the image
   */
  public getIconPath(name: string): string {
    return this.icons.get(name);
  }
}
