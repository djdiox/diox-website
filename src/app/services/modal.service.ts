import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {ModalOptions} from '../classes/models/modal-options';

@Injectable({
  providedIn: 'root'
})
/**
 * Provies Appwide Access to Modals
 */
export class ModalService {

  /**
   * Subject of the current Modal Options
   */
  private _currentModalOptions: Subject<ModalOptions>;

  /**
   * The last shown modal on the view
   */
  private options: ModalOptions;

  /**
   * Initializes this service
   */
  constructor() {
    this._currentModalOptions = new Subject<ModalOptions>();
  }

  /**
   * Returns an Observable for the currentModalOptions
   * @returns {Observable<ModalOptions>} Event to hook on
   * @constructor A fine Modal
   */
  public get CurrentModalOptions(): Observable<ModalOptions> {
    return this._currentModalOptions.asObservable();
  }

  /**
   * Shows the default modal on view by giving some parameters
   * @param name A unique name
   * @param title a title
   * @param content the content
   */
  public showDefaultModal(name: string, title: string, content: string) {
    const modalOptions = new ModalOptions(name, title, content, true);
    this.options = modalOptions;
    this._currentModalOptions.next(modalOptions);
  }

  /**
   * Hides the last modal
   * @param result {boolean} The result of the action
   */
  public hideCurrentModal(result = false) {
    this.options.visible = false;
    this.options.result = result;
    this._currentModalOptions.next(this.options);
  }
}
