import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
/**
 * A Pages services for sharing the current page
 */
export class InteractiveCvService {

  /**
   * Inner State, that can only be changed through setState
   */
  public initialState: any;
  /**
   * Visible CvPage
   */
  private stateSubject: Subject<any>;
  /**
   * The current State saved for sharing reasons
   */
  private currentState: any;

  /**
   * Creates a new Instance of the service
   */
  constructor() {
    this.stateSubject = new Subject<any>();
  }

  /**
   * CvPage Observable
   * @returns {Observable<any>} An Observable that will call everytime the page changes
   */
  public get CurrentState(): Observable<any> {
    return this.stateSubject.asObservable();
  }

  /**
   * Sets the state with the intital state
   * Alsways adds or replaces from the initial state
   * @param newState {object} The new state
   */
  public setState(newState: any) {
    this.currentState = _.assign(this.initialState, newState);
    this.stateSubject.next(_.clone(this.currentState));
  }

  /**
   * Adds the view to current breadcrumbs, or returns them
   * @param view{string} the view that has been set
   * @param breadcrumbs{[string]} the current breadcrumbs
   * @returns {[string]} the new current breadcrumbs
   */
  public getBreadcrumbs(view: string, breadcrumbs: string[]) {
    return breadcrumbs.indexOf(view) !== -1 ? breadcrumbs : [...breadcrumbs, view];
  }

}
