import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Page} from '../../classes/models/app-page';
import {appRoutes} from '../../routes';
import {NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';
import {LoadingService} from './loading.service';
import {ToasterService} from 'angular2-toaster';

@Injectable({
  providedIn: 'root'
})

/**
 * Handles the pages in the Application
 */
export class PageService {
  /**
   * The current view of the App
   */
  private _currentPage = new Subject<Page>();

  /**
   * Default current page for fast and easy access, instead of Observable
   */
  public CurrentPage = new Page('home');
  /**
   * A clone from the current routes
   */
  public routes = [];

  /**
   * Initializes this Service
   */
  constructor(
    private loadingService: LoadingService,
    private toastService: ToasterService,
    private router: Router
  ) {
    this.routes = JSON.parse(JSON.stringify(appRoutes));
    this._currentPage = new Subject<Page>();
    this._currentPage.subscribe(page => this.CurrentPage = page);
  }

  public getNextPage() {
    return this.routes[this.CurrentPage.data.index + 1];
  }

  /**
   * gets the previous route
   */
  public getPreviousPage() {
    return this.routes[this.CurrentPage.data.index - 1];
  }


  /**
   * Handles a Angular Route Change Event
   * 1. Show loading spinner
   * 2. Set current page
   * 3. Hide Spinner
   * 4. Show Toast on Error
   * @param event
   */
  public async handleRouteEvent(event) {
    if (event instanceof NavigationStart) {
      this.loadingService.showSpinner();
    }
    if (event instanceof NavigationEnd || event instanceof NavigationError) {
      const route = this.routes.find(appRoute => event.url.indexOf(appRoute.path) !== -1);
      if (event.url === '/') {
        return await this.router.navigate(['home']);
      }
      if (route) {
        this.setPage(new Page(route.path, route.data));
      }
      // Hide loading indicator
      this.loadingService.hideSpinner();
      if (event instanceof NavigationError) {
        // Present error to user
        this.toastService.pop('error', 'Naviagation to ' + event.url + ' made problems');
      }
    }
  }

  /**
   * Gets the current Page as Observable
   * @constructor
   */
  public get Page(): Observable<Page> {
    return this._currentPage.asObservable();
  }

  /**
   * Sets the current page
   * @param page {Page} the new current Page
   */
  public setPage(page: Page) {
    this._currentPage.next(page);
  }
}
