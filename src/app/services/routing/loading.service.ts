import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
/**
 * The service which handles loading requests
 */
export class LoadingService {

  /**
   * Indicates that the app is loading
   */
  private _isAppLoading: Subject<boolean>;

  /**
   * Instanciate Service for Dependency Injection
   * Default isAppLoading is false
   */
  constructor() {
    this._isAppLoading = new Subject();
    this._isAppLoading.next(false);
  }

  /**
   * Gets the IsAppLoading Status as an Observable with type boolen
   * @constructor
   */
  public get IsAppLoading(): Observable<boolean> {
    return this._isAppLoading.asObservable();
  }

  /**
   * Shows the loading Mask
   */
  public showSpinner() {
    this._isAppLoading.next(true);
  }

  /**
   * Hides the loading Mask
   */
  public hideSpinner() {
    this._isAppLoading.next(false);
  }
}
