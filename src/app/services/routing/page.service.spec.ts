import {inject, TestBed} from '@angular/core/testing';

import {PageService} from './page.service';
import {Page} from '../../classes/models/app-page';
import {ToasterService} from 'angular2-toaster';
import {LoadingService} from './loading.service';
import {NavigationEnd, NavigationError, NavigationStart} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {HomeComponent} from '../../pages/home/home.component';

describe('PageService', () => {
  const toasterServiceMock = {
    pop: jasmine.createSpy('pop')
  };
  const loadingServiceMock = {
    showSpinner: jasmine.createSpy('showSpinner'),
    hideSpinner: jasmine.createSpy('hideSpinner')
  };
  const routerMock = {
    navigate: jasmine.createSpy('navigate')
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [
        RouterTestingModule.withRoutes([{path: 'home', component: HomeComponent}])
      ],
      providers: [
        {
          provide: LoadingService,
          useValue: loadingServiceMock
        },
        {
          provide: RouterTestingModule,
          useValue: routerMock
        },
        PageService,
        {
          provide: ToasterService,
          useValue: toasterServiceMock
        }
      ]
    });
  });

  let pageService;
  beforeEach(inject([PageService], (service: PageService) => {
    pageService = service;
  }));

  it('should be created', async () => {
    await expect(pageService).toBeTruthy();
  });

  it('should hang on observable and set the page', async (done) => {
    const newPage = new Page('diamond', {someKey: 'someValue'});
    pageService.Page.subscribe(async (page: Page) => {
      await expect(page.view).toEqual(newPage.view);
      await expect(page.data).toEqual(newPage.data);
      done();
    });
    pageService.setPage(newPage);
  });

  it('should handle a Navigation Start event', async () => {
    const event = new NavigationStart(1, 'test');
    await pageService.handleRouteEvent(event);
    await expect(loadingServiceMock.showSpinner).toHaveBeenCalled();
  });

  it('should handle an NavigationEnd Event', async (done) => {
    const event = new NavigationEnd(2, 'home', 'another');
    pageService.Page
      .subscribe(async (page: Page) => {
        await expect(page.view).toEqual('home');
        await expect(page.data.header).toEqual('diox');
        done();
      });
    await pageService.handleRouteEvent(event);
    await expect(loadingServiceMock.hideSpinner).toHaveBeenCalled();
  });

  it('should handle an error', async () => {
    const event = new NavigationError(1, 'home', 'someError');
    await pageService.handleRouteEvent(event);
    await expect(toasterServiceMock.pop).toHaveBeenCalled();
  });
});
