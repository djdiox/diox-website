import { TestBed, inject } from '@angular/core/testing';

import { LoadingService } from './loading.service';

describe('LoadingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoadingService]
    });
  });

  let service;
  beforeEach(inject([LoadingService], (_service: LoadingService) => {
    service = _service;
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set is loading true', (done) => {
    service.IsAppLoading.subscribe((loading) => {
      expect(loading).toBeTruthy();
      done();
    });
    service.showSpinner();
  });


  it('should set is loading false', (done) => {
    service.IsAppLoading.subscribe((loading) => {
      expect(loading).toBeFalsy();
      done();
    });
    service.hideSpinner();
  });
});
