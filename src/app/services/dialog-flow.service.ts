import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {ApiAiClient} from 'api-ai-javascript';

@Injectable({
  providedIn: 'root'
})
/**
 * Natural Language Processing made easy
 * Learns by Artificial Intelligence
 */
export class DialogFlowService {
  /**
   * The baseURL From DialogFlow
   * @type {string}
   */
  private readonly client = new ApiAiClient({accessToken: environment.dialogFlowToken});

  /**
   * Initializes the Service for the DialogFlow API Connection
   */
  constructor() {
  }

  /**
   *
   * Gets the response for the DialogFlow Request
   * @param query {string} A string that should be searched in the AI
   * @returns
   */
  public async getResponse(query: string) {
    return await this.client.textRequest(query);
  }
}
