/**
 * The default response of any bot
 * @type string[]
 */
export const DefaultResponses = [
  'Sorry, i did not understand that!',
  'Can you try another sentence?',
  'That was too fast for me!',
  'Whoops, I did not get something',
  'Sorry, I am not that intelligent yet',
  'Have you tried "open settings" ?',
  'Maybe try help'
];

/**
 * An welcome Text for any visitor
 * @type string[]
 */
export const DefaultWelcomeTexts = [
  'Hey, thanks for visiting me!',
  'Awesome that you found me! Try \'help\' for more information',
  'Hello! Thanks that you came to talk to me!',
  'Hi there, nice for having you here!',
  'I am forever here, do you want to talk a bit?',
  'I\'m bored let\'s talk!'
];

