import {OperatingSystem, Platform, Tools} from './tools';

describe('Provider of the tools', function () {
  it('should get the navigator and the android OS', function () {
    const client =  Tools.getClient('android');
    expect(client.os).toEqual(OperatingSystem.Android);
    expect(client.platform).toEqual(Platform.Unknown);
    expect(client.mobile).toBeTruthy();
  });

  it('should get the iphone agent', function () {
    const client =  Tools.getClient('iphone');
    expect(client.os).toEqual(OperatingSystem.iOS);
    expect(client.platform).toEqual(Platform.Mobile);
    expect(client.mobile).toBeTruthy();
  });

  it('should get the ipad agent', function () {
    const client =  Tools.getClient('ipad');
    expect(client.os).toEqual(OperatingSystem.iOS);
    expect(client.platform).toEqual(Platform.Tablet);
    expect(client.mobile).toBeTruthy();
  });

  it('should get the windows agent', function () {
    const client =  Tools.getClient('windows');
    expect(client.os).toEqual(OperatingSystem.Windows);
    expect(client.platform).toEqual(Platform.Desktop);
    expect(client.mobile).toBeFalsy();
  });

  it('should get the mac agent', function () {
    const client =  Tools.getClient('mac');
    expect(client.os).toEqual(OperatingSystem.macOS);
    expect(client.platform).toEqual(Platform.Desktop);
    expect(client.mobile).toBeFalsy();
  });

  it('should get the linux agent', function () {
    const client =  Tools.getClient('linux');
    expect(client.os).toEqual(OperatingSystem.Linux);
    expect(client.platform).toEqual(Platform.Desktop);
    expect(client.mobile).toBeFalsy();
  });
});
