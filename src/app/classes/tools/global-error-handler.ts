import {ErrorHandler, Injectable} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {AuthService} from '../../services/user/auth.service';

/**
 * The Global Error Handler of the Application
 */
@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  /**
   * Creates the instance for this handler
   * @param toasterService {ToasterService} The Service of the Toaster
   * @param authService
   */
  constructor(
    private toasterService: ToasterService,
    private authService: AuthService
  ) {
  }

  /**
   * Handlers an global error thrown in error and makes a toast
   * @param error the error that has been thrown in The app
   */
  handleError(error) {
    // if (environment.production === false) {
    //    }
    console.error(error);
    if (this.authService && this.authService.LastAuthChanges.loggedIn &&
      this.authService.LastAuthChanges.currentUser.settings.showErrors) {
      this.toasterService
        .pop('error', 'An Error Occured',
          'An Error occured, be kind and let the developer know!');
      this.toasterService.pop(error.text);
    }
    // IMPORTANT: Rethrow the error otherwise it gets swallowed
    throw error;
  }

}
