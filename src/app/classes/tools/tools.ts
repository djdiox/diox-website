/**
 * The interface for a complete client
 */
export interface Client {
  /**
   * The OS of the client
   */
  os: OperatingSystem;
  /**
   * The Plattform of the the client
   */
  platform: Platform;
  /**
   * Is the client mobile?
   */
  mobile: boolean;
}

/**
 * An Enumeration for all current OS
 */
export enum OperatingSystem {
  /**
   * Not known
   */
  Unknown,
  /**
   * Google's Platform
   */
  Android,
  /**
   * Apples Platform
   */
  iOS,
  /**
   * Windows Platform
   */
  Windows,
  /**
   * Apple Platform
   */
  macOS,
  /**
   * Open Source alternative
   */
  Linux
}

/**
 * Also a enum for the current platforms
 */
export enum Platform {
  /**
   * Not known
   */
  Unknown,
  /**
   * A Home PC
   */
  Desktop,
  /**
   * Mobile Phone or similar
   */
  Mobile,
  /**
   * e.g. iPad
   */
  Tablet
}

/**
 * Just a colleciton of Tools that will be used app wide
 */
export class Tools {

  /**
   * Stops the bubbling event from Clicking or pressing a Button
   * Needs to be checked, because IE 11 does not support it
   * @param event {MouseEvent | KeyboardEvent} The Event from the action
   */
  static stopBubble(event: MouseEvent | KeyboardEvent) {
    if (typeof event.stopPropagation === 'function') {
      event.stopPropagation();
    }
  }

  /**
   * Check if the current User Agent is an Mobile Deveice
   * @param userAgent { : OperatingSystem, platform: Platform, mobile: boolean} represents a client system
   */
  static getClient(userAgent: string): Client {
    const client: Client = {
      os: OperatingSystem.Unknown,
      platform: Platform.Unknown,
      mobile: false
    };

    if (userAgent.toLowerCase().indexOf('android') !== -1) {
      client.os = OperatingSystem.Android;
      client.platform = Platform.Unknown;
      client.mobile = true;
    } else if (userAgent.toLowerCase().indexOf('iphone') !== -1) {
      client.os = OperatingSystem.iOS;
      client.platform = Platform.Mobile;
      client.mobile = true;
    } else if (userAgent.toLowerCase().indexOf('ipad') !== -1) {
      client.os = OperatingSystem.iOS;
      client.platform = Platform.Tablet;
      client.mobile = true;
    } else if (userAgent.toLowerCase().indexOf('windows') !== -1 &&
      userAgent.toLowerCase().indexOf('phone') === -1) {
      client.os = OperatingSystem.Windows;
      client.platform = Platform.Desktop;
    } else if (userAgent.toLowerCase().indexOf('mac') !== -1) {
      client.os = OperatingSystem.macOS;
      client.platform = Platform.Desktop;
    } else if (userAgent.toLowerCase().indexOf('linux') !== -1) {
      client.os = OperatingSystem.Linux;
      client.platform = Platform.Desktop;
    }
    return client;
  }
}
