/**
 * Type for the Appsettinngs
 */
export class Settings {

  /**
   * The address, where this frontend can find the specified backend
   */
  private backendUrl = 'http://localhost:3012';

  /**
   * Current User from the Settings
   */
  public user_id: string;

  /**
   * Selected Theme by the user
   */
  public theme = 'default';

  /**
   * Should errors in the App shown?
   */
  public showErrors = false;

  /**
   * Is the chat even visible?
   */
  public isChatVisible: boolean;

  /**
   * Is the chat switched on?
   */
  public isChatOnline: boolean;

  /**
   * Last openend pages
   */
  public visitedPages: string[] = [];

  /**
   * Saved Weblinks
   */
  public favorites: string[] = [];

  /**
   * If the user starts a track, it saves it here
   */
  public currentTrack: string;

  /**
   * Showing the settings?
   */
  public showSettings?: boolean;

  /**
   * The language of the user
   */
  public language = 'en';

  /**
   * Is Scroll Navigation Enabled?
   */
  public scrollNavigation = false;

  /**
   * Gets the last Page of the App
   */
  public get lastPage(): string {
    return this.visitedPages[this.visitedPages.length - 1];
  }

  /**
   * Creates a new instance of the class for the Settings
   * @param parameters
   */
  constructor(parameters: {
    user_id: string,
    currentPage?: string,
    chatOnline?: boolean,
    chatVisible?: boolean,
    scrollNavigation?: boolean,
    theme?: string,
    favorites?: string[],
    track?: string,
    backendUrl?: string,
  }) {
    const {user_id, theme, currentPage, chatOnline, chatVisible, favorites, track, backendUrl, scrollNavigation} = parameters;
    this.user_id = user_id;
    this.theme = theme ? theme : 'default';
    this.visitedPages = [
      ...this.visitedPages,
      currentPage
    ];
    this.scrollNavigation = scrollNavigation;
    this.isChatOnline = chatOnline;
    this.isChatVisible = chatVisible;
    this.favorites = favorites ? favorites : [];
    this.currentTrack = track;
    this.backendUrl = backendUrl;
  }
}
