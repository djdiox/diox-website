import {appRoutes} from '../../routes';

/**
 * An Model for the page in the app.
 */
export class Page {
  /**
   * The current view as String
   */
  view: string;

  /**
   * View Data, that might be needed sometimes
   */
  data: any;

  /**
   * Initializes a new Page with a name and data
   * @param _view {string} just  the name of the current visible view
   * @param _data {object} some data for the view
   */
  constructor(_view: string, _data?: object) {
    this.view = _view;
    if (_data) {
      this.data = _data;
    }
  }

  /**
   * Searches all routes for a specific word / page
   * @param keyword {string} the word or page you are looking for
   */
  public get Route() {
    return appRoutes.find(appRoute => this.view === appRoute.path);
  }
}
