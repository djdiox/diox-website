/**
 * Created a new Message for talking to the chatbot
 */
export class Message {
  /**
   * Text that will be send
   */
  public content: string;
  /**
   * Message Sent date
   */
  public timestamp: Date;

  /**
   * The avatar picture
   */
  public avatar: string;

  /**
   * Sender
   */
  public sender: string;

  /**
   * Has the message been read?
   */
  public markedAsRead = false;

  /**
   * Creates the message with given parameters
   * @param content {string} Text that will be send
   * @param avatar {string} The avatar picture
   * @param sender {string} The name of the Sender
   * @param timestamp {string} Message Sent date
   */
  constructor(content: string, sender: string, timestamp?: Date) {
    this.content = content;
    this.timestamp = timestamp || new Date();
    this.avatar = `assets/images/icons/${sender.toLowerCase()}.svg`;
    this.sender = sender;
  }
}
