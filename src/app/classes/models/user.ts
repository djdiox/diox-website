import {Settings} from './settings';
import {Client} from '../tools/tools';

/**
 * Represents a user with settings in the system
 */
export class User {
  /**
   * Unique ID of the user
   */
  public id: string;
  /**
   * the name of the user
   */
  public name: string;

  /**
   * an registered email address of the user‚
   */
  public email?: string;

  /**
   * an encrypted version of the password
   */
  public password?: string;

  /**
   * The last client the user used to access the page
   */
  public lastClient?: Client;
  /**
   * The settings from the User
   */
  public settings?: Settings;

  /**
   * The last login time of the user
   */
  public lastLogin?: Date;

  /**
   * The date when the user started to user the app
   */
  public registered?: Date;

  /**
   * Data from other services that have been used
   * to login to the App
   */
  public externalLoginData?: any;

  /**
   * Creates the user with a setting
   * @param id {string} the id of the user
   */
  constructor(id: string) {
    this.settings = new Settings({user_id: this.id});
  }
}
