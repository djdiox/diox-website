/**
 * The Modal Options that will create a new simple modal
 */
export class ModalOptions {

  /**
   * A Name to find the Modal
   */
  public name: string;

  /**
   * Visibile Header Element
   */
  public title: string;

  /**
   * Visible Content Element
   */
  public content: string;

  /**
   * Is it being displayed?
   */
  public visible: boolean;

  /**
   * Should it have buttons?
   */
  public interactive?: boolean;

  /**
   * Result if Interactive
   * @type {boolean}
   */
  public result = false;

  /**
   * Displayed Text on Yes Button
   */
  public buttonYesText?: string;

  /**
   * Displayed Text on No Button
   */
  public buttonNoText?: string;

  /**
   * Creates the instance of the class
   * Default Visible: false
   * @param name the name to find the modal again
   * @param header the header of the current modal
   * @param content the content (text) of the modal
   */
  constructor(name: string, title: string, content: string, visible = false) {
    this.name = name;
    this.title = title;
    this.content = content;
    this.visible = visible;
  }
}
