import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {Router} from '@angular/router';
import {AngularFireAuth} from 'angularfire2/auth';
import {RouterTestingModule} from '@angular/router/testing';
import {ToasterService} from 'angular2-toaster';
import {LoadingService} from './services/routing/loading.service';
import {AuthService} from './services/user/auth.service';
import {Subject} from 'rxjs';
import {MatBottomSheetModule, MatBottomSheetRef} from '@angular/material';
import {MockAuthService} from 'test/mocks/mock-auth-service';
import {IconsService} from './services/icons.service';
import createSpy = jasmine.createSpy;


let _eventMock = null, _currentUser = null;

const routerMock = {
  events: new Subject<Object>()
};

const matBottomSheetRefMock = {
  afterDismissed: () => {
    return {
      subscribe: jasmine.createSpy('sheetRefObservable', cb => cb())
    };
  },
  open: function () {
    return this;
  }
};

const angularFireAuthMock = {
  auth: {
    currentUser: _currentUser,
    signInWithPopup: createSpy('signWithPopup'),
    GoogleAuthProvider: function () {
      return '123';
    }
  }
};

const toasterServiceMock = {
  pop: createSpy('pop')
};

const loadingServiceMock = {
  showSpinner: createSpy('showSpinner'),
  hideSpinner: createSpy('hideSpinner'),
  IsAppLoading: {
    subscribe: createSpy('observable', cb => cb(false))
  }
};
const authServiceMock = new MockAuthService();
const iconsServiceMock = {
  registerIcons: jasmine.createSpy('registerIcons')
};

describe('AppComponent', async () => {
  beforeEach(async(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        {
          provide: Router,
          useValue: routerMock
        },
        {
          provide: AngularFireAuth,
          useValue: angularFireAuthMock
        },
        {
          provide: AuthService,
          useValue: authServiceMock
        },
        {
          provide: ToasterService,
          useValue: toasterServiceMock
        },
        {
          provide: LoadingService,
          useValue: loadingServiceMock
        },
        {
          provide: MatBottomSheetRef,
          useValue: matBottomSheetRefMock
        },
        {
          provide: IconsService,
          useValue: iconsServiceMock
        }
      ],
      imports: [
        RouterTestingModule,
        MatBottomSheetModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    }).compileComponents();
  }));


  let app;

  beforeEach(() => {
    _currentUser = null;
    _eventMock = null;
    routerMock.events = new Subject();
    const fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
  });

  it('should create the app', async(async () => {
    await expect(app).toBeTruthy();
    expect(iconsServiceMock.registerIcons).toHaveBeenCalled();
  }));

  it('should initialize the app and try to restore a user', async (done) => {
    app.ngOnInit();
    setTimeout(async () => {
      await expect(authServiceMock.restoreSession).toHaveBeenCalled();
      done();
    }, 300);
    await expect(loadingServiceMock.showSpinner).toHaveBeenCalled();
  });

  it('should get the state for the route', async(async () => {
    const outletMock = {
      activatedRouteData: {
        state: '1'
      }
    };
    const result = app.getState(outletMock);
    await expect(result).toEqual('1');
  }));

  it('should run the initialize the app', async(async () => {
    app.navToggled(true);
    await expect(app.state.showNav).toBe(true);
  }));

  // it('should open the login window', async () => {
  //   const keyEvent = new KeyboardEvent('keyup', {key: 'L'});
  //   app.keyEvent(keyEvent);
  //   await expect(authServiceMock.showLogin).toHaveBeenCalled();
  //   _currentUser = {name: 'someUser'};
  // });

  it('should open the login window', async () => {
    _currentUser = {name: 'someUser'};
    const keyEvent = new KeyboardEvent('keyup', {key: 'L'});
    app.keyEvent(keyEvent);
    await expect(authServiceMock.showLogin).toHaveBeenCalled();
  });

});
