import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {reverseRouterTransition, routerTransition} from '../animations';
import {LoadingService} from './services/routing/loading.service';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {PageService} from './services/routing/page.service';
import {AuthService} from './services/user/auth.service';
import {LoginComponent} from './pages/login/login.component';
import {MatBottomSheet} from '@angular/material';
import {IconsService} from './services/icons.service';

/**
 * Main Component of the App.
 * Works like a wrapper for every other page on the app.
 * Gets initialized in the root of the index.html file
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routerTransition, reverseRouterTransition] // register the animation
})
export class AppComponent implements OnInit, OnDestroy {

  /**
   * An object for the current state of the app.
   */
  public state = {
    lastKey: '',
    theme: 'sundown',
    showNav: true,
    lastPageIndex: 0,
    isFullScreenNavShown: false,
    loading: false,
    currentUser: null,
    subscriptions: [],
    scrolling: true,
  };

  /**
   * Configuration for angular2-toaster
   */
  public toasterConfig: ToasterConfig =
    new ToasterConfig({
      showCloseButton: true,
      positionClass: 'toast-top-center'
    });

  /**
   * Initializes the App Component
   * @param bottomSheet
   * @param router The Router Object by Angular
   * @param loadingService The loading service from this app
   * @param toastService the imported toaster service
   * @param authService an Instance of the Auth Service
   * @param iconsService the Service for all Icons
   * @param pageService a service for pages
   */
  constructor(
    private bottomSheet: MatBottomSheet,
    private router: Router,
    public loadingService: LoadingService,
    private toastService: ToasterService,
    private authService: AuthService,
    private iconsService: IconsService,
    private pageService: PageService
  ) {
    loadingService.IsAppLoading
      .subscribe(_loading => {
        this.state.loading = _loading;
      });
    this.iconsService.registerIcons();
    this.authService.AuthChanged.subscribe(authChanges => {
      this.state.currentUser = authChanges.currentUser;
      this.state.theme = this.state.currentUser.settings.theme;
      if (this.state.currentUser.settings.scrollNavigation) {
        window.scrollTo(0, 20);
        this.state.scrolling = false;
        document.body.style.height = '104%';
        window.addEventListener('scroll', this.scroll, true);
      } else {
        window.removeEventListener('scroll', this.scroll, true);
        document.body.style.height = '100%';
      }
      if (this.state.currentUser.settings.showSettings) {
        document.body.style.height = '100%';
      } else if (!this.state.currentUser.settings.showSettings && this.state.currentUser.settings.scrollNavigation) {
        document.body.style.height = '104%';
      }
    });
  }

  /**
   * Navigates to the given page and scrolls to the position 20 px in the middle
   * @param page {object} The object from the app routes
   */
  async navigateAndScroll(page: any) {
    if (!page) {
      return;
    }
    this.state.scrolling = true;
    await this.router.navigate(['/' + page.path]);
    window.scrollTo(0, 20);
    setTimeout(() => {
      window.scrollTo(0, 20);
      this.state.scrolling = false;
    }, 1000);
  }

  /**
   * Scrolling function, for route change on scroll
   */
  scroll = async () => {
    const scrollObject = {
      x: window.pageXOffset,
      y: window.pageYOffset
    };
    if (!this.state.scrolling) {
      if (scrollObject.y >= 30) {
        const nextPage = this.pageService.getNextPage();
        await this.navigateAndScroll(nextPage);
      } else if (scrollObject.y < 5) {
        const nextPage = this.pageService.getPreviousPage();
        await this.navigateAndScroll(nextPage);
      }
    }
  }

  /**
   * Initiate the Module (App)
   */
  ngOnInit() {
    this.loadingService.showSpinner();
    const routerEventSub = this.router.events
      .subscribe((event) =>
        this.pageService.handleRouteEvent(event));
    setTimeout(() => {
      if (this.authService.restoreSession()) {
        this.state.theme = this.authService.LastAuthChanges.currentUser.settings.theme;
        this.toastService
          .pop('success', 'Yarr! welcome back.',
            'The session has been restored for you!');
      }
    }, 200);
    const loginModalSub = this.authService.LoginModalToggled
      .subscribe(async () => {
        await this.showLoginModal();
      });
    this.state.subscriptions = [
      routerEventSub,
      loginModalSub
    ];
  }

  /**
   * Gets called when the app gets destroyed
   */
  ngOnDestroy() {
    this.state.subscriptions.forEach(sub => sub.unsubscribe());
    window.removeEventListener('scroll', this.scroll, true);
  }

  /**
   * Event hook for settings the emitted value from the nav-bar
   * @param {boolean} $event Is Current nav-bar shown?
   */
  public navToggled($event: boolean) {
    this.state.showNav = $event;
  }

  /**
   * Shows the Login Modal with BottomSheet
   */
  public async showLoginModal() {
    this.bottomSheet.open(LoginComponent);
  }

  /**
   * Gets the state of the current router outlet
   * @param outlet {Object} a RouterOutlet of the app
   */
  public getState(outlet) {
    this.state.lastPageIndex = outlet.activatedRouteData.index;
    let index = 0;
    if (
      this.pageService.CurrentPage &&
      this.pageService.CurrentPage.data
    ) {
      index = this.pageService.CurrentPage.data.index;
    }
    return this.state.lastPageIndex <
    index ? '' :
      outlet.activatedRouteData.state;
  }

  /**
   * Is The full screen nav shown?
   * @param $event The event of the action
   */
  public fullScreenNavToggled($event: boolean) {
    this.state.isFullScreenNavShown = $event;
  }

  /**
   * Gets called everytime the user presses a Button on the page
   * @param event {KeyboardEvent} The Event that has been passed from Angular
   */
  @HostListener('window:keyup', ['$event'])
  protected async keyEvent(event: KeyboardEvent) {
    event.preventDefault();
    this.state.lastKey = event.code;
    if (event.key === 'L') {
      await this.authService.showLogin();
    } else if (event.keyCode === 38) {
      await this.router.navigate(['/' + this.pageService.getNextPage().path]);
    } else if (event.keyCode === 39) {
      await this.router.navigate(['/' + this.pageService.getPreviousPage().path]);
    }
  }
}
