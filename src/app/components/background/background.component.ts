import {Component, ElementRef, OnInit} from '@angular/core';
import {ToasterService} from 'angular2-toaster';

@Component({
  selector: 'app-background',
  templateUrl: './background.component.html',
  styleUrls: ['./background.component.scss']
})
/**
 * The Background of the App.
 */
export class BackgroundComponent implements OnInit {

  /**
   * The current mode of the shark
   */
  public sharkMode = '';

  /**
   * Toggle Animation
   */
  public stopped = '';

  /**
   * Creates a new Background component
   */
  constructor(
    private toasterService: ToasterService,
    private elementRef: ElementRef
  ) {
  }

  /**
   * Angular Initialization Callback
   */
  ngOnInit() {
  }

  /**
   * Shows a shark message
   */
  public showSharkMessage() {
    this.toasterService.pop('success', 'I\'m Sharky! I will soon make some cool things');
  }

  /**
   * Toglles the Sharks transform
   */
  public transformShark() {
    const sharkyElement = this.elementRef.nativeElement.querySelector('#sharky');
    if (this.sharkMode === 'attacking') {
      return false;
    }
    this.sharkMode = 'attacking';
    sharkyElement.style.left = sharkyElement.offsetLeft + 'px';
    setTimeout(() => {
      sharkyElement.style.left = '105%';
      this.sharkMode = '';
    }, 5000);
  }
}
