import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BackgroundComponent} from './background.component';
import {ToasterService} from 'angular2-toaster';
import {MatTooltipModule} from '@angular/material';

describe('BackgroundComponent', () => {
  let component: BackgroundComponent;
  let fixture: ComponentFixture<BackgroundComponent>;

  const toasterServiceMock = {
    pop: jasmine.createSpy('pop')
  };
  beforeEach(async(async () => {
    await TestBed.configureTestingModule({
      providers: [
        {
          provide: ToasterService,
          useValue: toasterServiceMock
        }
      ],
      imports: [
        MatTooltipModule
      ],
      declarations: [BackgroundComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackgroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show the shark message', function () {
    component.showSharkMessage();
    expect(toasterServiceMock.pop).toHaveBeenCalled();
  });
});
