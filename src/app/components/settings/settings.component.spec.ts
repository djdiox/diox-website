import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettingsComponent} from './settings.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {
  MatBottomSheetModule,
  MatBottomSheetRef,
  MatButtonModule,
  MatExpansionModule,
  MatInputModule,
  MatSelectModule,
  MatSidenavModule,
  MatTooltipModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthService} from '../../services/user/auth.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {PageService} from '../../services/routing/page.service';
import {MockAngularFireAuth} from '../../../../test/mocks/mock-angular-fire';
import {MockAuthService} from '../../../../test/mocks/mock-auth-service';
import {User} from '../../classes/models/user';
import {ToasterService} from 'node_modules/angular2-toaster';
import {FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;
  const authServiceMock = new MockAuthService();
  beforeEach(async(async() => {
    await TestBed.configureTestingModule({
      declarations: [
        SettingsComponent
      ],
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        MatSidenavModule,
        MatExpansionModule,
        MatInputModule,
        MatButtonModule,
        MatTooltipModule,
        MatSelectModule,
        FormsModule
      ],
      providers: [
        {
          provide: AngularFireAuth,
          useValue: MockAngularFireAuth
        },
        PageService,
        {
          provide: AuthService,
          useValue: authServiceMock
        },
        ToasterService
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
      .compileComponents();
  }));
  let closeSideNav, showSideNav;

  beforeEach(() => {
    authServiceMock.reset();
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    showSideNav = spyOn(component.sideNav, 'open').and.returnValue(Promise.resolve()).and.callFake(async() => {});
    fixture.detectChanges();
    closeSideNav = spyOn(component.sideNav, 'close').and.returnValue(Promise.resolve()).and.callFake(async() => {});
  });

  it('should create', async () => {
    await expect(component).toBeTruthy();
  });

  // xit('should initialize the Component without logged in user', async () => {
  //   authServiceMock.isLoggedIn.and.returnValue(false);
  //   await component.ngOnInit();
  //   expect(authServiceMock.AuthChanged.subscribe).toHaveBeenCalled();
  //   expect(component.state.subscriptions.length).toEqual(2);
  //   expect(showSideNav).not.toHaveBeenCalled();
  // });

  // it('should initialize the Component with logged in user', async () => {
  //   authServiceMock.LastAuthChanges.currentUser.settings.showSettings = true;
  //   authServiceMock.isLoggedIn.and.returnValue(true);
  //   component.ngOnInit();
  //   expect(authServiceMock.AuthChanged.subscribe).toHaveBeenCalled();
  //   expect(component.state.subscriptions.length).toEqual(2);
  //   expect(showSideNav).toHaveBeenCalled();
  // });

  it('Should unsubscribe on destroy', async () => {
    const unsubscribeSpy = jasmine.createSpy('unsubscribe');
    component.state.subscriptions = [
      {unsubscribe: unsubscribeSpy},
      {unsubscribe: unsubscribeSpy},
      {unsubscribe: unsubscribeSpy}
    ];
    component.ngOnDestroy();
    await expect(unsubscribeSpy).toHaveBeenCalledTimes(3);
  });

  it('should set chat to offline if invisible', async () => {
    component.state.user = new User('test123');
    component.state.user.settings.isChatOnline = true;
    component.onChatVisibilityChanged(false);
    await expect(component.state.user.settings.isChatOnline).toBeFalsy();
  });

  // xit('should open login if tried to open settings', async () => {
  //   // TODO Find out how to inject Fake Component
  //   const spy = spyOn(matBottomSheetRefMock, 'open').and.callThrough();
  //   await component.openSettings();
  //   expect(spy).toHaveBeenCalled();
  // });
  //
  it('should open settings', async (done) => {
    component.state.user = new User('test123');
    await component.showMenu();
    await expect(showSideNav).toHaveBeenCalled();
    await expect(authServiceMock.assignSettings).toHaveBeenCalledWith({showSettings: true});
    done();
  });

  it('should close the menu', async (done) => {
    await component.close();
    await expect(authServiceMock.assignSettings).toHaveBeenCalledWith({showSettings: false});
    done();
  });
});
