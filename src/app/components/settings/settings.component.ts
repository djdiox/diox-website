import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatBottomSheet, MatSidenav} from '@angular/material';
import {LoginComponent} from '../../pages/login/login.component';
import {PageService} from '../../services/routing/page.service';
import {User} from '../../classes/models/user';
import {AuthChanged, AuthService} from '../../services/user/auth.service';
import {ToasterService} from 'angular2-toaster';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
/**
 * Managing settings
 */
export class SettingsComponent implements OnInit, OnDestroy {

  /**
   * Current visible State of the settings
   */
  public state = {
    showSettings: false,
    waitingForLogin: false,
    page: '',
    user: <User>null,
    subscriptions: [],
    changeSettingSubscription: null,
    statusText: {true: 'online', false: 'offline'},
    visibleText: {true: 'visible', false: 'hidden'},
    errorText: {true: 'errors are shown', false: 'errors are hidden'},
    scrollText: {true: 'Scroll Navigation is Enabled', false: 'Scroll Navigation is disabled'}
  };
  /**
   * The ViewChild of the App, that stores a reference to the Side Navigation
   */
  @ViewChild('sideNav') sideNav: MatSidenav;

  /**
   * Creates the Component, and injects the settings
   * @param pageService {PageService} Page Service of the app.
   * @param toasterService
   * @param authService {AuthService} Authentication Service
   */
  constructor(
    private pageService: PageService,
    private toasterService: ToasterService,
    private authService: AuthService,
  ) {
  }

  /**
   * Called if Component got loaded
   */
  public async ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.state.user = this.authService.LastAuthChanges.currentUser;
    }
    const s1 = this.authService.AuthChanged.subscribe(async (changed: AuthChanged) => {
      if (changed.loggedIn === false) {
        return;
      }
      this.state.user = changed.currentUser;
      if ((!this.state.showSettings &&
        this.state.user.settings.showSettings) ||
        this.state.waitingForLogin === true) {
        this.state.waitingForLogin = false;
        await this.showMenu();
      }
    });
    const s2 = this.pageService.Page
      .subscribe(page => {
        this.state.page = page.view;
      });
    this.state.subscriptions = [s1, s2];
  }

  /**
   * Gets called if the component gets destroyed
   */
  ngOnDestroy() {
    this.state.subscriptions.forEach(sub => sub.unsubscribe());
  }

  /**
   * Logs the user out of the app
   */
  public async logout() {
    this.authService.logout();
    this.toasterService.pop('success', 'You have been logged out of the app.');
    window.location.reload();
  }

  /**
   * switches online to offline if visibility is false
   * @param visible {boolean} Is the current chat visible?
   */
  public onChatVisibilityChanged(visible: boolean) {
    if (!visible) {
      this.state.user.settings.isChatOnline = false;
    }
  }

  /**
   * Opens the settings with the last time set options
   */
  public async openSettings() {
    if (this.state.user && this.state.user.settings) {
      return await this.showMenu();
    }
    this.state.waitingForLogin = true;
    this.authService.showLogin();
  }

  /**
   * Closes the sigeNav
   */
  public async close() {
    this.state.showSettings = false;
    await this.authService.assignSettings({showSettings: false}).toPromise();
    if (this.state.changeSettingSubscription) {
      this.state.changeSettingSubscription.unsubscribe();
    }
    await this.sideNav.close();
  }

  /**
   * Saves the current settings to the app
   */
  public async save() {
    await this.authService.assignSettings(this.state.user.settings).toPromise();
  }

  /**
   * Shows the side navigation
   */
  public async showMenu() {
    if (!this.state.user ||
      !this.state.user.settings) {
      return;
    }
    this.state.showSettings = true;
    await this.sideNav.open();
    this.state.changeSettingSubscription =
      this.authService
        .assignSettings({showSettings: true})
        .subscribe(() => {
          this.toasterService.pop('success',
            'Saved & applied settings!');
        });
  }
}
