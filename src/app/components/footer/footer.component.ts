import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
/**
 * Sticky Footer for the bottom of the page
 */
export class FooterComponent implements OnInit {

  /**
   * Is the fullscreen Nav visible?
   */
  @Input() fullScreenNavShown = false;

  /**
   * The current visible player
   */
  public currentPlayer = 'none';

  /**
   * Class Initialization
   */
  constructor() {
  }

  /**
   * Angular Component Initialization Method
   */
  ngOnInit() {
  }

  /**
   * Sets the current player by the social-icons
   * @param player
   */
  public setCurrentPlayer(player: string) {
    this.currentPlayer = player;
  }
}

