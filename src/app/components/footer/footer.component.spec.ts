import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FooterComponent} from './footer.component';
import {InlineSVGModule} from 'ng-inline-svg';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MatIconModule} from '@angular/material';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async(async () => {
    await TestBed.configureTestingModule({
      declarations: [FooterComponent],
      providers: [],
      imports: [
        HttpClientTestingModule,
        MatIconModule,
        InlineSVGModule.forRoot(),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', async () => {
    await expect(component).toBeTruthy();
    await expect(component.currentPlayer).toEqual('none');
  });
  it('should set the current player', async () => {
    const nextPlayer = 'mixcloud';
    component.setCurrentPlayer(nextPlayer);
    await expect(component.currentPlayer).toEqual(nextPlayer);
  });
});
