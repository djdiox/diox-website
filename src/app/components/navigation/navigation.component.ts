import {Component, EventEmitter, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {appRoutes} from '../../routes';
import {Page} from '../../classes/models/app-page';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
/**
 * A navbar for redirecting through the site
 */
export class NavigationComponent implements OnInit {

  /**
   * Event that will be triggered if the user presses the nav toggle button
   * @type {EventEmitter<boolean>}
   */
  @Output() navToggled = new EventEmitter<boolean>();

  /**
   * gets Called everytime a page has been changed
   */
  @Output() pageChanged = new EventEmitter<Page>();

  /**
   * Gets called if the full screen nav visibility has changed
   */
  @Output() fullScreenNavToggled = new EventEmitter<boolean>();

  /**
   * The current routes in our app
   */
  public routes: string[] = [];

  /**
   * Is this nav visible?
   * @type {boolean}
   */
  public showNav = true;

  /**
   * For Mobile we will see a fullscreen navigation
   */
  public showFullscreenNav = false;

  /**
   * Class Initialization
   */
  constructor() {
    this.routes = appRoutes
      .map(route => route.path)
      .filter(view => view !== '');
  }

  /**
   * Angular Component Initialization Method
   */
  ngOnInit() {
  }

  /**
   * Toggles the navbar, and emits the event
   */
  public toggleNav() {
    this.showNav = !this.showNav;
    this.navToggled.emit(this.showNav);
  }

  /**
   * Shows or hides the mobile navigation
   */
  public toggleMobileNav() {
    this.showFullscreenNav = !this.showFullscreenNav;
    this.fullScreenNavToggled.emit(this.showFullscreenNav);
  }
}
