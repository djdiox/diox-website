import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SocialIconsComponent} from './social-icons.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MatIconModule} from '@angular/material';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {InlineSVGModule} from 'ng-inline-svg';

describe('SocialIconsComponent', () => {
  let component: SocialIconsComponent;
  let fixture: ComponentFixture<SocialIconsComponent>;
  const eventMock = {
    stopPropagation: jasmine.createSpy('stopPropagination'),
    altKey: '',
    buttons: ''
  };
  beforeEach(async(async () => {
    await TestBed.configureTestingModule({
      declarations: [SocialIconsComponent],
      imports: [
        HttpClientTestingModule,
        MatIconModule,
        InlineSVGModule.forRoot(),
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
      .compileComponents();
  }));
  let originalTimeout;
  beforeEach(() => {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  afterEach(function () {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it('should create', async () => {
    await expect(component).toBeTruthy();
  });

  it('should show all social Icons', async () => {
    component.showSocialIcons(eventMock);
    await expect(component.socialVisible).toEqual(true);
    await expect(eventMock.stopPropagation).toHaveBeenCalled();
  });

  it('should hide all social Icons', async () => {

    component.socialVisible = true;
    component.hideSocialIcons(eventMock);
    await expect(component.socialVisible).toEqual(false);
    await expect(eventMock.stopPropagation).toHaveBeenCalled();
  });

  it('should  open the url and hide icons', async () => {
    window.open = jasmine.createSpy('window_open');
    component.openSocial('www.google.de');
    await expect(window.open).toHaveBeenCalledWith('www.google.de');
    expect(component.socialVisible).toEqual(false);
  });

  it('should show the player', async () => {
    await component.togglePlayer('mixcloud', eventMock);
    await expect(component.currentPlayer).toEqual('mixcloud');
  });

  it('should hide the player', async () => {
    component.currentPlayer = 'mixcloud';
    await component.togglePlayer('mixcloud', eventMock);
    await expect(component.currentPlayer).toEqual('none');
  });

  it('should show the current play icon', async () => {
    const player = 'mixcloud';
    component.togglePlayIcon(player, true);
    await expect(component.activePlayIcon).toEqual('mixcloud');
  });

  it('should jide the current play icon', async () => {
    const player = 'mixcloud';
    component.activePlayIcon = 'mixcloud';
    component.togglePlayIcon(player, false);
    await expect(component.activePlayIcon).toEqual('');
  });

});
