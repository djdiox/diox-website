import {Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import {IconsService} from '../../services/icons.service';
import * as _ from 'lodash';
import {Tools} from '../../classes/tools/tools';

export class SocialIcon {
  public name: string;
  public url: string;
  public path: string;
  public hasPlayer: boolean;

  constructor(name: string, url: string, hasPlayer = false) {
    this.name = name;
    this.url = url;
    this.hasPlayer = hasPlayer;
  }
}

@Component({
  selector: 'app-social-icons',
  templateUrl: './social-icons.component.html',
  styleUrls: ['./social-icons.component.scss']
})

/**
 * An Container for all Links to the Social Icons
 */
export class SocialIconsComponent implements OnInit {
  /**
   * The complete list of social icons
   */
  public visibleSocialIcons = [
    new SocialIcon('mixcloud', 'https://www.mixcloud.com/diox_dj/', true),
    new SocialIcon('soundcloud', 'https://soundcloud.com/nerdreflex', true),
    new SocialIcon('facebook', 'https://www.facebook.com/djdi0x/', false),
    new SocialIcon('twitter', 'https://twitter.com/markuswagner93', false),
    new SocialIcon('instagram', 'https://www.instagram.com/dj.diox/', false),
    new SocialIcon('gitlab', 'https://gitlab.com/djdiox', false),
    new SocialIcon('github', 'https://github.com/djdiox', false)
  ];

  /**
   * The Current Icon where the Play Icon is visible
   */
  public activePlayIcon = '';

  /**
   * Sets the current player of the app.
   */
  @Output() playerChanged = new EventEmitter<string>();

  /**
   * The current position of the button
   */
  @Input() position = 'top';

  /**
   * Are the social Buttons visible?
   * @type {boolean}
   */
  public socialVisible = false;

  /**
   * Shows the current Player on the view
   */
  public currentPlayer = '';

  /**
   * Creates an Instance of the social icons container
   */
  constructor(
    private iconsService: IconsService
  ) {
    this.visibleSocialIcons = _.map(this.visibleSocialIcons, icon => {
      icon.path = this.iconsService.getIconPath(icon.name);
      return icon;
    });
  }

  /**
   * Angular On Init function, when the component is ready
   */
  ngOnInit() {
  }

  /**
   * Toggles the given play icon over the social icon
   * @param val {string} the player where the icon should be shown
   * @param show {boolean} show or hide the icon
   */
  public togglePlayIcon(val: string, show: boolean) {
    if (this.activePlayIcon === val && show) {
      return;
    }
    this.activePlayIcon = show ? val : '';
  }

  /**
   * Toggles the list of social links
   * @param event
   */
  showSocialIcons(event) {
    this.socialVisible = !this.socialVisible;
    Tools.stopBubble(event);
  }

  /**
   * Hides the buttons if the user clicks somewhere else in the page
   * @param event
   */
  @HostListener('window:click', ['$event'])
  hideSocialIcons(event) {
    if (!this.socialVisible) {
      return;
    }
    this.socialVisible = false;
    Tools.stopBubble(event);
  }

  /**
   * Opens the specified URL in a new window
   * @param url the link that should be opened
   */
  openSocial(url) {
    this.socialVisible = false;
    window.open(url);
  }

  /**
   * shows the player by an Icon
   * @param player
   * @param event
   */
  public togglePlayer(player: string, event: any) {
    this.currentPlayer = this.currentPlayer === player ? 'none' : player;
    this.playerChanged.emit(this.currentPlayer);
    Tools.stopBubble(event);
  }
}
