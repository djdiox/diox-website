import {Component, ElementRef, HostListener, OnDestroy, ViewChild} from '@angular/core';
import {Message} from '../../classes/models/message';
import {DialogFlowService} from '../../services/dialog-flow.service';
import {fromEvent, Subscription} from 'rxjs';
import * as _ from 'lodash';
import {clone} from 'lodash';
import {debounceTime, map} from 'rxjs/operators';
import {AuthService} from '../../services/user/auth.service';
import {ToasterService} from 'angular2-toaster';
import {MatDialog, MatExpansionPanel} from '@angular/material';
import {environment} from '../../../environments/environment';
import {DefaultResponses, DefaultWelcomeTexts} from '../../classes/static/bot-texts';
import {Tools} from '../../classes/tools/tools';
import {ChatHelpComponent} from './chat-help/chat-help.component';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})

/**
 * A chat component for implementing dialog flow for user interaction
 */
export class ChatComponent implements OnDestroy {

  /**
   * The selector for the message input
   */
  private readonly MESSAGE_SELECTOR = '#message';
  /**
   * Name of the "User" of the Chat
   */

  private readonly USER_NAME = 'User';

  /**
   * The name of the bot in the App
   */
  private readonly BOT_NAME = 'Bot';

  /**
   * Event name of the Key Up of an input
   */
  private readonly KEY_UP_EVENT = 'keyup';

  /**
   * The ViewChild of the Expansion Panel
   */
  @ViewChild('chatControl') chat: MatExpansionPanel;

  /**
   * The State of the chat
   */
  public state = {
    chatVisible: false,
    typing: [],
    unreadMessages: 0,
    user: {
      loggedIn: false,
      messages: [],
      message: <Message>null
    },
    messages: [],
    subscriptions: []
  };

  /**
   * Creates a instance of this component
   * @param dialogFlowService The Service for connecting dialog flow into the app
   * @param authService The auth service for the logged in User
   * @param toastService
   * @param dialog
   * @param elementRef an Ref of this Element on the view.
   */
  constructor(
    private dialogFlowService: DialogFlowService,
    public authService: AuthService,
    private toastService: ToasterService,
    public dialog: MatDialog,
    private elementRef: ElementRef,
  ) {
    this.saveMessage(ChatComponent.getRandomInArray(DefaultWelcomeTexts), this.BOT_NAME);
    this.resetMessage();
    this.setUnreadMessages();
  }

  /**
   * The Array to be searched for
   * @param array {string[]} The Array to be searched
   * @returns {string} The Response for the text
   */
  private static getRandomInArray(array: string[]) {
    return array[_.random(0, array.length - 1)];
  }

  /**
   * resolves an action if necessary
   * @param action {string} the resolved query from the current action
   */
  private resolveBotAction(action: string) {
    switch (action) {
      case 'app.current.settings.open':
        this.authService.assignSettings({showSettings: true});
        return true;
      case 'app.current.close':
        this.chat.close();
        return true;
      case 'app.logout':
        this.authService.logout();
        setTimeout(location.reload(), 1500);
        return true;
      case 'app.current.version':
        this.toastService.pop('success', 'Current Version is: ' + environment.version);
        return true;
      case 'app.current.history.clear':
        setTimeout(() => {
          this.state.messages = [];
        }, 1500);
        return true;
      default:
        return false;
    }
  }

  /**
   * Resets the current Message to a placeholder, without text
   */
  private resetMessage() {
    this.state.user.message = new Message('', this.USER_NAME);
  }

  /**
   * Adds an multiple subscriptions to the state
   * @param newSubs {Subscription[]} The new subscriptions for destroying
   */
  private addSubscriptions(newSubs: Subscription[]) {
    this.state.subscriptions = [...this.state.subscriptions, newSubs];
  }

  /**
   * Sets the count of the unread messages on the badge
   */
  private setUnreadMessages() {
    this.state.unreadMessages =
      this.state.messages
        .filter(message => message.markedAsRead === false).length;
  }

  /**
   * Marks all messages unread
   */
  private markAllMessagesRead() {
    this.state.messages = this.state.messages.map(message => {
      return {
        ...message,
        markedAsRead: true
      };
    });
    this.setUnreadMessages();
  }

  /**
   * Sets the text of an tyiping user
   * @param name {string} the name that should be displayed
   */
  private setTyping(name: string) {
    if (this.state.typing.indexOf(name) !== -1) {
      return;
    }
    this.state.typing = [...this.state.typing, name];
  }

  /**
   * Removes a name from the "Typing" text
   * @param name {string} the user to be removed
   */
  private removeTyping(name: string) {
    this.state.typing =
      this.state.typing.filter(prop => prop !== name);
  }

  /**
   * Sends a message via model binding & dialogFlow Service
   */
  public async sendMessage(content: string, isValid: boolean) {
    if (!isValid) {
      return this.toastService.pop('warning', 'Please enter a message to send!');
    }
    this.saveMessage(content, this.USER_NAME);
    this.resetMessage();
    await this.getBotMessage(content);
  }

  /**
   * Initializes the typing field above the input
   */
  private initTyping() {
    this.state.chatVisible = true;
    const currentInput = this.elementRef.nativeElement.querySelector(this.MESSAGE_SELECTOR);
    const inputEvent$ = fromEvent(currentInput, this.KEY_UP_EVENT)
      .pipe(map(ev => (<HTMLInputElement>(<KeyboardEvent> ev).target).value));
    const inputSub = inputEvent$
      .subscribe(() => {
        this.setTyping(this.USER_NAME);
        this.markAllMessagesRead();
      });
    const debouncedInputSub = inputEvent$.pipe(debounceTime(400))
      .subscribe(() => this.removeTyping(this.USER_NAME));
    this.addSubscriptions([inputSub, debouncedInputSub]);
    setTimeout(() => {
      document.getElementById('message').focus();
    }, 200);
  }

  /**
   * Angular On Destroy of Component Method
   */
  ngOnDestroy() {
    this.state.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  /**
   * Hides the buttons if the user clicks somewhere else in the page
   * @param event
   */
  @HostListener('window:click', ['$event'])
  hideChat(event) {
    if (
      event.target.closest('.chat-control') ||
      this.state.chatVisible === false
    ) {
      return;
    }
    this.state.chatVisible = false;
    this.chat.close();
  }

  /**
   * Saves the messages locally so the the chat history gets not lost
   * @param content {string} the content of the message
   * @param sender {string} the sender of the message
   */
  public saveMessage(content: string, sender: string) {
    const current = new Message(content, sender, new Date());
    current.markedAsRead = this.state.chatVisible;
    this.state.messages.push(current);
    setTimeout(() => {
      const panelBody = this.elementRef.nativeElement.querySelector('.chat-list');
      if (panelBody) {
        panelBody.scrollTop = panelBody.scrollHeight;
      }
    }, 50);
  }

  /**
   * Gets a message from the bot if needed
   * @param userMessage
   */
  private async getBotMessage(userMessage: string) {
    this.setTyping(this.BOT_NAME);
    const result = await this.dialogFlowService.getResponse(userMessage);
    const botMessage = result.result.fulfillment.speech;
    const nextMessage = botMessage === '' ? ChatComponent.getRandomInArray(DefaultResponses) : botMessage;
    this.saveMessage(nextMessage, this.BOT_NAME);
    this.removeTyping(this.BOT_NAME);
    this.resolveBotAction(result.result.action);
    this.setUnreadMessages();
  }

  /**
   * Initializes a debounced event for displaying the "is typing" dialog
   */
  public chatOpened() {
    this.state.chatVisible = true;
    this.initTyping();
    this.markAllMessagesRead();
    this.setUnreadMessages();
  }

  /**
   * Closes the chat
   */
  public closeChat() {
    this.state.chatVisible = false;
  }

  /**
   * Shows the Help for the Chatbot!
   * @param event
   */
  public showHelp(event: any) {
    const dialogRef = this.dialog.open(ChatHelpComponent, {
      // width: '250px',
      autoFocus: true,
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result) {
        await this.sendMessage(result, true);
      }
      this.state.chatVisible = true;
    });
    Tools.stopBubble(event);
  }
}
