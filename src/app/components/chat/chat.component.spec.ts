import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatComponent} from './chat.component';
import {DialogFlowService} from '../../services/dialog-flow.service';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {CUSTOM_ELEMENTS_SCHEMA, ElementRef, NO_ERRORS_SCHEMA} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {AuthService} from '../../services/user/auth.service';
import {MockAngularFireAuth} from 'test/mocks/mock-angular-fire';
import {AngularFireAuth} from 'angularfire2/auth';
import {MockAuthService} from '../../../../test/mocks/mock-auth-service';
import {DefaultWelcomeTexts} from '../../classes/static/bot-texts';
import {MAT_DIALOG_DATA, MatDialog, MatDialogModule, MatDialogRef} from '@angular/material';
import {ChatHelpComponent} from './chat-help/chat-help.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserDynamicTestingModule} from '@angular/platform-browser-dynamic/testing';


describe('ChatComponent', () => {
  let component: ChatComponent;
  let fixture: ComponentFixture<ChatComponent>;
  let resMock = null, dialogResult = null;
  const dialogFlowResult = {
    result: {
      fulfillment: {
        speech: 'test Message'
      }
    }
  };
  const dialogFlowServiceMock = {
    getResponse: jasmine.createSpy('getResponse').and
      .returnValue(dialogFlowResult)
  };
  const toasterServiceMock = {
    pop: jasmine.createSpy('pop')
  };
  const elementRefMock = {};
  const authServiceMock = new MockAuthService();
  authServiceMock.LastAuthChanges.loggedIn = true;
  authServiceMock.LastAuthChanges.currentUser.settings.isChatVisible = true;
  const matDialogObservableMock = {
    subscribe: jasmine.createSpy('subscribe')
  };
  const matDialogRefMock = {
    open: () => {
      return {
        afterClosed: () => {
          return matDialogObservableMock;
        }
      };
    }
  };
  beforeEach(async(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ChatComponent,
        ChatHelpComponent
      ],
      providers: [
        {
          provide: AngularFireAuth,
          useValue: MockAngularFireAuth
        },
        {
          provide: DialogFlowService,
          useValue: dialogFlowServiceMock
        },
        {
          provide: AuthService,
          useValue: authServiceMock
        },
        {
          provide: ToasterService,
          useValue: toasterServiceMock
        },
        {
          provide: ElementRef,
          useValue: elementRefMock
        },
        {
          provide: MatDialogRef,
          useValue: matDialogRefMock
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: 'Testdata'
        },
        {
          provide: MatDialog,
          useValue: matDialogRefMock
        }
      ],
      imports: [
        FormsModule,
        MatDialogModule,
        HttpClientModule,
        BrowserAnimationsModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [ChatHelpComponent]
      }
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', async () => {
    await expect(component).toBeTruthy();
    await expect(component.state.messages[0].sender).toEqual('Bot');
    await expect(component.state.messages[0].markedAsRead).toEqual(false);
    const welComeText = component.state.messages[0].content;
    await expect(DefaultWelcomeTexts).toContain(welComeText);
  });

  it('should unsubscribe to any subscription in the component', async () => {
    const unsubscribeSpy = jasmine.createSpy('unsubscribe');
    component.state.subscriptions = [
      {
        unsubscribe: unsubscribeSpy
      },
      {
        unsubscribe: unsubscribeSpy
      }
    ];
    component.ngOnDestroy();
    await expect(unsubscribeSpy).toHaveBeenCalledTimes(2);
  });

  it('should send a Message and save it', async () => {
    resMock = {
      result: {
        fulfillment: {
          speech: 'test Response'
        }
      }
    };
    const myMessage = 'Test Text';
    component.saveMessage(myMessage, 'test');
    await expect(component.state.messages.length).toEqual(2);
    const message = component.state.messages[1];
    await expect(message.markedAsRead).toEqual(component.state.chatVisible);
    await expect(message.content).toEqual(myMessage);
    await expect(message.timestamp instanceof Date).toBeTruthy();
    await expect(message.sender).toEqual('test');
  });

  it('should close the chat', async () => {
    component.state.chatVisible = true;
    component.closeChat();
    await expect(component.state.chatVisible).toBeFalsy();
  });

  it('should not a send a empty message', async () => {
    await component.sendMessage('test', false);
    await expect(toasterServiceMock.pop).toHaveBeenCalledWith('warning', 'Please enter a message to send!');
  });

  it('should send a Message', async () => {
    const testContent = 'test';
    await component.sendMessage(testContent, true);
    await expect(dialogFlowServiceMock.getResponse).toHaveBeenCalledWith(testContent);
    await expect(component.state.messages.length).toEqual(3);
    const botMessage = component.state.messages[2];
    await expect(botMessage.content).toEqual(dialogFlowResult.result.fulfillment.speech);
  });

  it('should open the help in a dialog', (done) => {
    component.state.messages = [];
    dialogResult = 'Open Settings';
    matDialogObservableMock.subscribe.and.callFake(async (cb) => {
      await cb(dialogResult);
      await expect(component.state.chatVisible).toEqual(true);
      await expect(component.state.messages.length).toEqual(2);
      done();
    });
    const eventMock = {
      preventDefault: jasmine.createSpy('preventDefault')
    };
    component.showHelp(eventMock);
  });
});
