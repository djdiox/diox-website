import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatHelpComponent} from './chat-help.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef, MatListModule} from '@angular/material';
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';

describe('ChatHelpComponent', () => {
  let component: ChatHelpComponent;
  let fixture: ComponentFixture<ChatHelpComponent>;
  const dialogResult = null;
  const dialogRefMock = {
    afterClosed: jasmine.createSpy().and.returnValue({
      subscribe: (cb => cb(dialogResult))
    })
  };
  const matDialogRefMock = {
    open: jasmine.createSpy('open').and.returnValue(dialogRefMock),
    close: jasmine.createSpy('close')
  };
  beforeEach(async(async () => {
    await TestBed.configureTestingModule({
      providers: [
        {
          provide: MatDialogRef,
          useValue: matDialogRefMock
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: 'Testdata'
        }
      ],
      imports: [
        MatDialogModule,
        MatListModule
      ],
      declarations: [ChatHelpComponent],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', async () => {
    await expect(component).toBeTruthy();
  });
  it('should close on no', function () {
    component.onNoClick();
    expect(matDialogRefMock.close).toHaveBeenCalled();
  });
});
