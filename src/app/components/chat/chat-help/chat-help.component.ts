import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  templateUrl: './chat-help.component.html',
  styleUrls: ['./chat-help.component.scss']
})
export class ChatHelpComponent implements OnInit {

  /**
   * Creates a new instance from the Chat Help Dialog
   * @param dialogRef {MatDialogRef<ChatHelpComponent>} the Ref for showing the page in a dialog
   * @param result
   */
  constructor(
    public dialogRef: MatDialogRef<ChatHelpComponent>,
    @Inject(MAT_DIALOG_DATA) public result: string
  ) {
  }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
