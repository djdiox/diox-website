import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TabBarComponent} from './tab-bar.component';

describe('TabBarComponent', () => {
  let component: TabBarComponent;
  let fixture: ComponentFixture<TabBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabBarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabBarComponent);
    component = fixture.componentInstance;
    component.tabs = ['test1', 'test2'];
    fixture.detectChanges();
  });

  it('should create', async () => {
    await expect(component).toBeTruthy();
  });

  it('should select a tab', async () => {
    component.current = component.tabs[0];
    component.select(component.tabs[1]);
    await expect(component.current).toEqual(component.tabs[1]);
  });
});
