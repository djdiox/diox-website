import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
/**
 * The Loader Component of the App
 */
export class LoaderComponent implements OnInit {

  /**
   * Typescript Constructor
   */
  constructor() { }

  /**
   * Angular Initialization callback
   */
  ngOnInit() {
  }

}
