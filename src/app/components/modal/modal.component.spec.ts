import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {ModalComponent} from './modal.component';
import {ModalService} from '../../services/modal.service';
import {ModalOptions} from '../../classes/models/modal-options';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;

  beforeEach(async(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModalComponent],
      providers: [
        ModalService
      ]
    })
      .compileComponents();
  }));

  let modalService;
  beforeEach(inject([ModalService], (_service: ModalService) => {
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    modalService = _service;
  }));

  it('should create', async () => {
    await expect(component).toBeTruthy();
  });

  it('should set current modal options on change', async (done) => {
    modalService.CurrentModalOptions.subscribe(async () => {
      await expect(component.CurrentModalOptions.name).toEqual('test');
      await expect(component.CurrentModalOptions.title).toEqual('test');
      await expect(component.CurrentModalOptions.content).toEqual('test');
      done();
    });
    modalService.showDefaultModal('test', 'test', 'test');
  });
  it('should close the modal', async (done) => {
    modalService.showDefaultModal('test', 'test', 'test');
    modalService.CurrentModalOptions.subscribe(async (_modalOptions: ModalOptions) => {
      await expect(_modalOptions.visible).toEqual(false);
      await expect(_modalOptions.result).toEqual(true);
      done();
    });
    component.closeModal(true);
  });
});
