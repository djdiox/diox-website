// Angular Imports
import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
// Angular Requirements
import {environment} from '../environments/environment';
import {appRoutes} from './routes';
// External Imports
import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {AngularFireStorageModule} from 'angularfire2/storage';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {InlineSVGModule} from 'ng-inline-svg';
import {
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatCardModule,
  MatChipsModule,
  MatDialogModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatProgressBarModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatTooltipModule
} from '@angular/material';
import {ToasterModule} from 'angular2-toaster';
import {GalleryModule} from '@ngx-gallery/core';
// Components
import {BackgroundComponent} from './components/background/background.component';
import {NavigationComponent} from './components/navigation/navigation.component';
import {TabBarComponent} from './components/tab-bar/tab-bar.component';
import {ModalComponent} from './components/modal/modal.component';
import {ChatComponent} from './components/chat/chat.component';
import {FooterComponent} from './components/footer/footer.component';
import {LoaderComponent} from './components/loader/loader.component';
import {SettingsComponent} from './components/settings/settings.component';
import {SocialIconsComponent} from './components/social-icons/social-icons.component';
// Services
import {LoadingService} from './services/routing/loading.service';
// Pages
import {AppComponent} from './app.component';
import {HomeComponent} from './pages/home/home.component';
import {PicturesComponent} from './pages/pictures/pictures.component';
import {AboutComponent} from './pages/about/about.component';
import {SetsComponent} from './pages/sets/sets.component';
import {WorkComponent} from './pages/work/work.component';
import {BlogComponent} from './pages/blog/blog.component';
import {InteractiveCvComponent} from './pages/interactive-cv/interactive-cv.component';
import {CvStartComponent} from './pages/interactive-cv/cv-start/cv-start.component';
import {CvSchoolComponent} from './pages/interactive-cv/cv-school/cv-school.component';
import {CvExperienceComponent} from './pages/interactive-cv/cv-experience/cv-experience.component';
import {CvTechnologyComponent} from './pages/interactive-cv/cv-technology/cv-technology.component';
import {CvProgrammingComponent} from './pages/interactive-cv/cv-programming/cv-programming.component';
import {CvGoalsComponent} from './pages/interactive-cv/cv-goals/cv-goals.component';
import {CvEndComponent} from './pages/interactive-cv/cv-end/cv-end.component';
import {LoginComponent} from './pages/login/login.component';
import {ChatHelpComponent} from './components/chat/chat-help/chat-help.component';
// Classes
import {GlobalErrorHandler} from './classes/tools/global-error-handler';

/**
 * Ng Module for this App
 */
@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomeComponent,
    PicturesComponent,
    AboutComponent,
    SetsComponent,
    WorkComponent,
    FooterComponent,
    BlogComponent,
    TabBarComponent,
    ModalComponent,
    ChatComponent,
    InteractiveCvComponent,
    CvStartComponent,
    CvSchoolComponent,
    CvExperienceComponent,
    CvTechnologyComponent,
    CvProgrammingComponent,
    CvGoalsComponent,
    CvEndComponent,
    LoaderComponent,
    SettingsComponent,
    LoginComponent,
    SocialIconsComponent,
    BackgroundComponent,
    ChatHelpComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false,
        useHash: true,
      } // <-- debugging purposes only
    ),
    InlineSVGModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    MatButtonModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatChipsModule,
    MatBadgeModule,
    MatListModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatBottomSheetModule,
    MatSelectModule,
    MatSidenavModule,
    MatDialogModule,
    ToasterModule.forRoot(),
    GalleryModule.forRoot({
      loop: true,
      autoPlay: false,
      playerInterval: 1000000,
      thumb: false
    })
  ],
  providers: [
    LoadingService,
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    }
  ],
  entryComponents: [AppComponent, LoginComponent, ChatHelpComponent],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule {
}
