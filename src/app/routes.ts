import {HomeComponent} from './pages/home/home.component';
import {Routes} from '@angular/router';
import {AboutComponent} from './pages/about/about.component';
import {SetsComponent} from './pages/sets/sets.component';
import {PicturesComponent} from './pages/pictures/pictures.component';
// import {WorkComponent} from './pages/work/work.component';
import {InteractiveCvComponent} from './pages/interactive-cv/interactive-cv.component';

/**
 * An Array of all the routes in our app
 * For a clean overview in a new file
 * @type Array
 */

/**
 * Current App Routes in the App
 * @type Array
 */
export const appRoutes: Routes = [

  {
    path: 'home',
    component: HomeComponent,
    data: {
      state: 'home',
      header: 'diox',
      index: 0
    }
  },
  {
    path: 'about',
    component: AboutComponent,
    data: {
      state: 'about',
      header: 'About me',
      index: 1
    }
  },
  {
    path: 'sets',
    component: SetsComponent,
    data: {
      state: 'sets',
      header: 'diox',
      index: 2
    }
  },
  {
    path: 'pictures',
    component: PicturesComponent,
    data: {
      state: 'pictures',
      header: 'Image Gallery',
      index: 3
    }
  },
  // {
  //   path: 'work',
  //   component: WorkComponent,
  //   data: {
  //     state: 'work',
  //     header: 'Work'
  //   }
  // },
  // {
  //   path: 'blog',
  //   component: BlogComponent,
  //   data: {
  //     state: 'blog',
  //     header: 'Blog'
  //   }
  // },
  {
    path: 'cv',
    component: InteractiveCvComponent,
    data: {
      state: 'cv',
      header: 'CV\'s can be more than a PDF!',
      index: 4
    }
  },
  {
    path: '',
    component: HomeComponent,
    data: {
      state: 'home',
      header: 'diox',
      index: 5
    }
  },
  /*{
   path: '**',
   component: PageNotFoundComponent
   }*/
];
