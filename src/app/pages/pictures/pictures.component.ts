import {Component, OnInit} from '@angular/core';
import {GalleryItem, ImageItem} from '@ngx-gallery/core';

@Component({
  selector: 'app-pictures',
  templateUrl: './pictures.component.html',
  styleUrls: ['./pictures.component.scss']
})

/**
 * Controller for the Pictures (Gallery) Page
 */
export class PicturesComponent implements OnInit {

  /**
   * The Images of the gallery
   */
    // public images = [
    //   {
    //     title: 'Picture ',
    //     src: 'assets/images/photos/photo_1.jpg',
    //     width: Math.round(Math.floor(Math.random() * 100))
    //   },
    //   {
    //     title: 'Picture 2',
    //     src: 'assets/images/photos/photo_2.jpg',
    //     width: Math.round(Math.floor(Math.random() * 100))
    //   },
    //   {
    //     title: 'Picture 3',
    //     src: 'assets/images/photos/photo_3.jpg',
    //     width: Math.round(Math.floor(Math.random() * 100))
    //   }
    // ];
  images: GalleryItem[];

  /**
   * The current count of images loaded
   */
  public imagesLoaded = false;

  /**
   * Instance Created
   */
  constructor() {
  }

  /**
   * Angular Initialization Method
   */
  ngOnInit() {
    this.images = [
      new ImageItem({src: 'assets/images/photos/photo_1.jpg', thumb: 'assets/images/photos/photo_1.jpg'}),
      new ImageItem({src: 'assets/images/photos/photo_2.jpg', thumb: 'assets/images/photos/photo_2.jpg'}),
      new ImageItem({src: 'assets/images/photos/photo_3.jpg', thumb: 'assets/images/photos/photo_3.jpg'}),
      new ImageItem({src: 'assets/images/photos/photo_4.jpg', thumb: 'assets/images/photos/photo_4.jpg'}),
      new ImageItem({src: 'assets/images/photos/photo_5.jpg', thumb: 'assets/images/photos/photo_5.jpg'}),
      // ... more items
    ];
    this.imagesLoaded = true;
  }
}
