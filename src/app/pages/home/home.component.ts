import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
/**
 * Controller for the Home CvPage
 */
export class HomeComponent implements OnInit {

  /**
   * Class Initialization
   */
  constructor() {

  }

  /**
   * Angular Component Initialization Method
   */
  public ngOnInit() {
  }
}
