import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {FormsModule} from '@angular/forms';
import {
  MatBottomSheetModule,
  MatBottomSheetRef,
  MatButtonModule,
  MatExpansionModule,
  MatInputModule,
  MatSidenavModule,
  MatTooltipModule
} from '@angular/material';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AngularFireAuth} from 'angularfire2/auth';
import {AuthService} from '../../services/user/auth.service';
import {MockAngularFireAuth} from '../../../../test/mocks/mock-angular-fire';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  const matBottomSheetRefMock = {
    dismiss: jasmine.createSpy('dismissBottomSheet')
  };
  const angularfireMock = {};

  const authServiceMock = {
    login: jasmine.createSpy('login').and.returnValue(true)
  };
  const toasterServiceMock = {
    pop: jasmine.createSpy('pop')
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        MatExpansionModule,
        MatSidenavModule,
        MatInputModule,
        MatButtonModule,
        MatBottomSheetModule,
        MatTooltipModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
      providers: [
        {
          provide: MatBottomSheetRef,
          useValue: matBottomSheetRefMock
        },
        {
          provide: AngularFireAuth,
          useValue: MockAngularFireAuth
        },
        {
          provide: AuthService,
          useValue: authServiceMock
        },
        {
          provide: ToasterService,
          useValue: toasterServiceMock
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', async () => {
    await expect(component).toBeTruthy();
  });

  it('should save the form', async () => {
    await component.saveForm('test@test.de', 'test', true, {
      preventDefault: jasmine.createSpy('preventDefault')
    });
    await expect(authServiceMock.login).toHaveBeenCalledWith('test@test.de', 'test');
  });

  it('should show an error on login failed', async (done) => {
    authServiceMock.login.and.throwError('Invalid Password').and.returnValue(Promise.reject('Invalid Password'));
    await component.saveForm('test@test.de', 'test', true);
    await expect(toasterServiceMock.pop).toHaveBeenCalledWith('error', 'Can\'t login to Server', 'Invalid Password');
    done();
  });
});
