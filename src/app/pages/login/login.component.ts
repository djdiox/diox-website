import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef, MatIconRegistry } from '@angular/material';
import { ToasterService } from 'angular2-toaster';
import { AuthService } from '../../services/user/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { auth } from 'firebase';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
/**
 * The Component for logging into the own account
 */
export class LoginComponent implements OnInit {

  public user = {
    name: '',
    password: ''
  };

  /**
   * Creates the instance with DI for Component
   * @param bottomSheetRef {MatBottomSheetRef<LoginComponent>>} The Ref to the LoginComponent Wrapped in BottomSheet
   * @param authService
   * @param toasterService {ToasterService} The service that will be show toasts
   * @param iconRegistry {MatIconRegistry} AngularMaterial Icon Registry
   */
  constructor(
    private bottomSheetRef: MatBottomSheetRef<LoginComponent>,
    private authService: AuthService,
    public afAuth: AngularFireAuth,
    private toasterService: ToasterService,
  ) {
}

  /**
   * Gets called when the component is finally initialized
   */
  ngOnInit() {
  }

  /**
   * If the user presses the Google Login Button,
   * this is the callback, when the login succeded on Google
   */
  public async openGoogleLogin() {
    try {
      const response = await this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
      if (!response) {
        return;
      }
      this.authService.loginFromGoogle(response);
      this.bottomSheetRef
      .dismiss(true);
    } catch (e) {
      this.toasterService.pop('error', 'Error at Logging in', e);
    }
  }

  /**
   * Saves the form and closes the bottom sheets
   * @param username {string} entered username
   * @param password {string} the password of the user
   * @param formValid {boolean} Is the form valid like this?
   * @param event {Event} Click Event from the user
   * @returns toast {Toast} the toast of the action
   */
  public async saveForm(username: string, password: string, formValid: boolean, event?: any) {
    if (!formValid) {
      return this.toasterService.pop('error', 'Invalid Form', 'Please check form for missing / invalid fields.');
    }
    let result = false, error = null;
    try {
      result = await this.authService.login(username, password);
    } catch (e) {
      error = e;
      result = false;
    }

    if (!result || error) {
      console.error(error);
      return this.toasterService.pop('error',
        'Can\'t login to Server',
        error);
    }
    this.bottomSheetRef
      .dismiss(true);
    if (event && typeof event.preventDefault === 'function') {
      event.preventDefault();
    }
    return this.toasterService.pop('success', `User ${username} has been logged in!`);
  }
}
