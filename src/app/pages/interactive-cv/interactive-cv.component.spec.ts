import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {InteractiveCvComponent} from './interactive-cv.component';
import {
  MatButtonModule,
  MatCardModule,
  MatChipsModule,
  MatIconModule,
  MatListModule,
  MatProgressBarModule,
  MatTooltipModule
} from '@angular/material';
import {InteractiveCvService} from '../../services/interactive-cv.service';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {DEFAULT_STATE} from './_presets/cv-default-state';

describe('InteractiveCvComponent', () => {
  let component: InteractiveCvComponent;
  let fixture: ComponentFixture<InteractiveCvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
      declarations: [InteractiveCvComponent],
      imports: [
        MatButtonModule,
        MatTooltipModule,
        MatProgressBarModule,
        MatChipsModule,
        MatListModule,
        MatIconModule,
        MatCardModule
      ],
      providers: [
        InteractiveCvService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InteractiveCvComponent);
    component = fixture.componentInstance;
    component.state = JSON.parse(JSON.stringify(DEFAULT_STATE));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should switch to next page', () => {
    component.nextPage();
    expect(component.state.index).toEqual(1);
  });

  it('should switch to previous page', () => {
    component.nextPage();
    component.nextPage();
    component.previousPage();
    expect(component.state.index).toEqual(1);
  });
});
