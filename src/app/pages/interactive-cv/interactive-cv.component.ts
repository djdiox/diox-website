import {Component, OnInit} from '@angular/core';
import * as _ from 'lodash';
import {InteractiveCvService} from '../../services/interactive-cv.service';
import {DEFAULT_STATE} from './_presets/cv-default-state';
import {CvPage} from './_presets/cv-page';

@Component({
  selector: 'app-interactive-cv',
  templateUrl: './interactive-cv.component.html',
  styleUrls: ['./interactive-cv.component.scss']
})
/**
 * Controller for the Interactive CV
 */
export class InteractiveCvComponent implements OnInit {

  /**
   * The state of the CV, e.g. Data
   */
  public state = _.clone(DEFAULT_STATE);

  /**
   * Is the current CV loading?
   */
  public loading = false;

  /**
   * Class Initialization
   * @param interActiveCvService {InteractiveCvService} The Service of the CV
   */
  constructor(private interActiveCvService: InteractiveCvService) {
    this.loading = true;
    this.interActiveCvService
      .CurrentState
      .subscribe(_state => {
        this.state = _.assign(this.state, _state);
        this.loading = false;
      });

    this.interActiveCvService.setState(DEFAULT_STATE);
  }

  /**
   * Angular Component Initialization Method
   */
  ngOnInit() {
  }

  /**
   * Shows the next page
   */
  public nextPage() {
    this.switchTo(this.state.index + 1);
  }

  /**
   * Shows the previous page
   */
  public previousPage() {
    this.switchTo(this.state.index - 1);
  }

  /**
   * switches the page the a index
   * @param index the index for switching
   */
  public switchTo(index: number) {
    this.loading = true;
    const view = this.state.data.views[index];
    if (this.state.view === view || typeof view === 'undefined') {
      return;
    }
    const page = new CvPage({
      view: view,
      index: index,
      breadcrumbs: this.interActiveCvService.getBreadcrumbs(view, this.state.breadcrumbs),
      viewCount: this.state.data.views.length - 1
    });
    this.interActiveCvService.setState(page);
  }
}
