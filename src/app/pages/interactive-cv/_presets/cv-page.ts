
/**
 * A typical page in the app
 */
export class CvPage {

  /**
   * Name of the view
   */
  public view: string;

  /**
   * Index of the current view (for pagination)
   */
  public index: number;

  /**
   * The progress of the currentPage
   */
  public progress: number;

  /**
   * The displayed breadcrumbs
   */
  public breadcrumbs: string[];

  /**
   * Generates the CvPage of the CV by given Parameters
   * @param parameters {object} An constructed Parameters Object, that is being used.
   */
  constructor(parameters: {
    view: string,
    index: number,
    breadcrumbs?: Array<string>,
    viewCount?: number
  }) {
    const {view, index, breadcrumbs, viewCount} = parameters;
    this.view = view;
    this.index = index;
    this.breadcrumbs = breadcrumbs;
    if (typeof viewCount !== 'undefined') {
      this.progress = index / viewCount * 100;
    }
  }

}
