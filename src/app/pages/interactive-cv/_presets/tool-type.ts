/*
 * Represents the type of tools
 */
export enum ToolType {
  OS = 'OS',
  IDE = 'IDE',
  framework = 'framework',
  database = 'database',
  office = 'office',
  management = 'management',
  development = 'development'
}


/**
 * Just the keys of the types
 */
const types = Object.keys(ToolType);

/**
 * The collected types of tools
 */
export const Types = types;
