import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-cv-start',
  templateUrl: './cv-start.component.html',
  styleUrls: ['./cv-start.component.scss']
})
/**
 * Controller for the CV (Start) CvPage
 */
export class CvStartComponent implements OnInit {

  /**
   * Class Initialization
   */
  constructor() {
  }

  /**
   * Angular Component Initialization Method
   */
  ngOnInit() {
  }

}
