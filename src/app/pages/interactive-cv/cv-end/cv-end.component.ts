import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-cv-end',
  templateUrl: './cv-end.component.html',
  styleUrls: ['./cv-end.component.scss']
})
/**
 * Controller for the CV (End) CvPage
 */
export class CvEndComponent implements OnInit {

  /**
   * Instance Created
   */
  constructor() {
  }

  /**
   * Angular Initialization Method
   */
  ngOnInit() {
  }

}
